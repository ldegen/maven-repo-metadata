=====================================
Extract Repository Metadata from POMs
=====================================

I started this as an attempt to solve `an issue I encountered with Reposilite <https://github.com/dzikoysk/reposilite/issues/476>`_
after realizing that generating *correct* metadata is not as straight forward a task as I initially anticipated. :-)

ATM this is but a couple of notes and some experimental JUnit-test cases.  I am
starting to build an actual library from this, that Reposilite (and potentially
other applications) could use

Initial Thoughts
================

- Extracting correct metadata requires examining the effective model of a pom -
  otherwise we cannot evaluate expressions. Expressions may be used to
  configure the plugin prefix mapping. It seems to be a commeon (recommended
  even) practice to use references to properties here to avoid duplication.
  Those properties, however, may very well be defined in some parent pom... you
  get where I am going with this.

- Generating the effective model from the raw xml data is a non-trivial task
  and probably best left to maven itself. I want to create a library that
  tries to utilize a minimal subset of maven-core to achieve this *without*
  running the complete maven plexus shebang, which I think would be
  prohibitively costly in terms of memory/cpu consumption.

- I think I can pull this of by providing custom
  ModelResolver/WorkspaceModelResolver implementations. Those would completely
  ignore any repositories defined in the POMs itself.  Instead, I am working on
  the premise that everything existing in our local repository is "The World" -
  and nothing outside the world exists.  This *will* break some cases, I am
  pretty sure, but we have to make a cut somewhere, don't we.


(Ab-)using Maven's `DefaultModelBuilder`
========================================

In `maven-model-builder` I found the class
`org.apache.maven.model.building.DefaultModelBuilder` and in particular its
Method `build(ModelBuildingRequest)`. Those pretty much do what it sais on the
bin: they build models. *Effective* ones even. My current approach is to
provide this method with its most basic needs and then let it do its thing.

Resolving Model Sources and Models
==================================

Maven processes a single `pom.xml` file into a "raw" model. My understanding
is, that this is not much more than a syntactic model of the POM, i.e. no
expressions have been evaluated, and references to other POMs (parents mostly?
not 100% sure here) are not yet resolved. Creating an actual ("effective")
model involves resolving those references. Onses those are resolved and linked
into our model, we can evaluate most expressions -- not all, since in theory
expressions can reference global settings, the process environment, and so
forth.  I think we can ignore those cases for the use case extractin repository
metadata.

The `ModelBuildingRequest` contains two strategy-esque objects to support the
resolution process.

The interface `org.apache.maven.model.resolution.ModelResolver` is defined in
`maven-model-builder`.  Its job is to lookup the *source code* for a given POM
based on its coordinates i.e. `groupId`, `artifactId` and `version`.

The Resolver interface is designed in a "stateful" way: As far as I can see,
one instance is used per build, and it contains API to dynamically add
repository definitions that are encountered when processing a raw model.  *This
is the part, that I intent to silently ignore!* Instead, we need a Resolver
that solely resolves POMs within some local storage.

In Reposilite, a new/updated MetadataService would provide an adapter that
translates those coordinates to `java.nio.file.Path` instances or similar, and
connect to the repository-specific Storage implementation.

Another important integredient is the
`org.apache.maven.model.resolution.WorkspaceModelResolver`.  The ModelBuilder
uses it to lookup raw and effective models that have already been
parsed/processed.  I am not 100% sure, but for our purposes, I think we can use
it as a high-level cache. I am probably missing most of the intricacies
involved here - let's just say this is my working hypothesis.


Some random observations
========================

There are three types of metadata records:

- group metadata lives in a directory corresponding to a groupId. Maven CLI uses this metadata to find plugins and their goal prefix mappings.

- artifact metadata live in a directory correpsonding to an artifact. Maven uses this in some situations e.g. during dependency conflict mediation
  to figure out which versions exist for that artifact. It also uses this to find the latest known version of some artifact, although relying on this
  is frowned upon. (I wonder why that might be :-) )

- snapshot metadata live in a directory corresponding to a snapshot version of some artifact. Maven uses this to figure out what the latest
  uploaded version of some file for said snapshot might be. But why is it necessary to track different versions of the same *snapshot*?
  I never undertood this particular aspect, but to many ppl it seems to be very important.

In theory, those types could overlap, since groupIds are not guaranteed to be prefix-free. I have not yet seen this in real life, though.
In my current implementation, I think this could cause some weird validation error messages, though this probably could be fixed without much of a problem.
(TODO: write a test case for this)

If the maven-metadata for some directory needs to be generated, all required information
is in the same directory or below.

All resources we are interested in can be found by constructing a path of the following form::

  <Path for groupId> "/" <artifactId> "/" <version> "/" <artifactId> "-" <subversion> "." <extension>

``Path for groupId`` is constructed by replacing dots (".") with slashes ("/").

``subversion`` is the same as ``version`` for "Release"-Artifacts. However for "Snapshot"-Artifacts,
it is constructed like this::

  <target version> "-" <timestamp> "-" <build number>

Here, ``target version`` is the prefix of the version string before the ``-SNAPSHOT``. ``timestamp`` is a string
of the form ``yyyymmdd.hhmmss`` and ``build number`` is a an integral number that is incremented with each upload (I think).

The ``subversion`` corresponds with the contents of the ``metadata/versioning/snapshotVersions/snapshotVersion/value``-Elements.

Also, the contents of ``metadata/versioning/snapshot/timestamp`` and ``metadata/versioning/snapshot/buildNumber`` correspond
with the respective ``timestamp`` and ``build number`` values for the most recently uploaded artifacts.


Modus Operandi
==============

If metadata for some directory needs to be generated.

- scan for paths below that directory that match the pattern described above.

- for each path we generate a metadata record for the artifact level. If the path points to a
  POM and that POM has packaging ``maven-plugin`` we also generate metadata for the group level.
  If the path points to some snapshot artifact, we additionally generate metadata for the snapshot level.

- If several records end up at the same path, those records are merged.


When generating group-level metadata, we need to actually examine the POM to determine the correct prefix mapping.
For this we create the so called "effective pom" to be able to resolve placeholder expressions and such. To do so,
references to a parent pom may need to be resolved (even recursively, I think). If those referencs are to snapshot versions
of that parent, we need the respective snapshot metadata first. This should not be much of a problem, it only means that
the whole process will start recursively for the snapshot directory of said parent.

If there is some other way to a priori determine the transitive closure of some project wrt the parent-child relation, an alternative
would be to just do a topo sort first and then process each pom in the "correct" order. I am not aware of any such way, though.
