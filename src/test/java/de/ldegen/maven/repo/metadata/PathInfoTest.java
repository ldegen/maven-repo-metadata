package de.ldegen.maven.repo.metadata;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.nio.file.Path;
class PathInfoTest {

  /*
   * Path <-- <Path for groupId> "/" <artifactId> "/" <version> "/" <artifactId> "-" Subversion "-" <classifier> "." <extension>
   * 
   * Subversion <-- <target version> "-" <timestamp> "-" <build number>
   *            <-- <version>
   */
  
  @Test
  void testSnapshot() {
   
    Path path = Path.of("de","ldegen","maven","test","foo-bar","1.0-SNAPSHOT","foo-bar-1.0-20210524.201923-42.jar");
    PathInfo pi = PathInfo.of(path);
    assertEquals(path,pi.getPath());
    assertEquals(path.getParent(),pi.getVersionPath());
    assertEquals(path.getParent().getParent(),pi.getArtifactPath());
    assertEquals(path.getParent().getParent().getParent(),pi.getGroupPath());
    assertEquals("de.ldegen.maven.test",pi.getGroupId());
    assertEquals("foo-bar", pi.getArtifactId());
    assertEquals("1.0-SNAPSHOT",pi.getVersion());
    assertEquals("1.0-20210524.201923-42",pi.getSubversion());
    assertEquals("jar",pi.getExtension());
    assertEquals("1.0",pi.getTargetVersion());
    assertEquals("20210524.201923",pi.getTimestamp());
    assertNull(pi.getClassifier());
    assertEquals(42,pi.getBuildNumber());
  }
  
  @Test
  void testRelease() {
    Path path = Path.of("de","ldegen","maven","test","foo-bar","1.0","foo-bar-1.0.jar");
    PathInfo pi = PathInfo.of(path);
    assertEquals(path,pi.getPath());
    assertEquals(path.getParent(),pi.getVersionPath());
    assertEquals(path.getParent().getParent(),pi.getArtifactPath());
    assertEquals(path.getParent().getParent().getParent(),pi.getGroupPath());
    assertEquals("de.ldegen.maven.test",pi.getGroupId());
    assertEquals("foo-bar", pi.getArtifactId());
    assertEquals("1.0",pi.getVersion());
    assertEquals("1.0",pi.getSubversion());
    assertEquals("jar",pi.getExtension());
    assertEquals("1.0",pi.getTargetVersion());
    assertNull(pi.getTimestamp());
    assertNull(pi.getClassifier());
    assertNull(pi.getBuildNumber());
  }

  @Test
  void testClassifier() {
    Path path = Path.of("de","ldegen","maven","test","foo-bar","1.0","foo-bar-1.0-jar-with-dependencies.jar");
    PathInfo pi = PathInfo.of(path);
    assertEquals(path,pi.getPath());
    assertEquals(path.getParent(),pi.getVersionPath());
    assertEquals(path.getParent().getParent(),pi.getArtifactPath());
    assertEquals(path.getParent().getParent().getParent(),pi.getGroupPath());
    assertEquals("de.ldegen.maven.test",pi.getGroupId());
    assertEquals("foo-bar", pi.getArtifactId());
    assertEquals("1.0",pi.getVersion());
    assertEquals("1.0",pi.getSubversion());
    assertEquals("jar",pi.getExtension());
    assertEquals("1.0",pi.getTargetVersion());
    assertNull(pi.getTimestamp());
    assertEquals("jar-with-dependencies",pi.getClassifier());
    assertNull(pi.getBuildNumber());
  }
  
  @Test
  void testSillyCase() {
    Path path = Path.of("de","ldegen","foo-bar","1.0-baz-bang","foo-bar-1.0-baz-bang-jar-with-dependencies.jar");
    PathInfo pi = PathInfo.of(path);
    assertEquals(path,pi.getPath());
    assertEquals(path.getParent(),pi.getVersionPath());
    assertEquals(path.getParent().getParent(),pi.getArtifactPath());
    assertEquals(path.getParent().getParent().getParent(),pi.getGroupPath());
    assertEquals("de.ldegen",pi.getGroupId());
    assertEquals("foo-bar", pi.getArtifactId());
    assertEquals("1.0-baz-bang",pi.getVersion());
    assertEquals("1.0-baz-bang",pi.getSubversion());
    assertEquals("jar",pi.getExtension());
    assertEquals("1.0-baz-bang",pi.getTargetVersion());
    assertNull(pi.getTimestamp());
    assertEquals("jar-with-dependencies",pi.getClassifier());
    assertNull(pi.getBuildNumber());
  }
  
  @Test
  void testConstructPaths() {
    PathInfo pi = PathInfo.of("de.ldegen.maven.test","foo-bar","1.0");
    assertEquals(Path.of("de","ldegen","maven","test","foo-bar","1.0","foo-bar-1.0.jar"), pi.getPath());
    assertEquals(Path.of("de","ldegen","maven","test","foo-bar","1.0","foo-bar-1.0-jar-with-dependencies.jar"), pi.withClassifier("jar-with-dependencies").getPath());
    assertEquals(Path.of("de","ldegen","maven","test","foo-bar","1.0","foo-bar-1.0.pom"), pi.withExtension("pom").getPath());
    
    PathInfo pi2 = PathInfo.of("de.ldegen.maven.test","foo-bar","1.0-SNAPSHOT","20201231.111111",42);
    assertEquals(Path.of("de","ldegen","maven","test","foo-bar","1.0-SNAPSHOT","foo-bar-1.0-20201231.111111-42.jar"),pi2.getPath());
    assertThrows(IllegalArgumentException.class, ()->PathInfo.of("de.ldegen.maven.test","foo-bar","1.0-SNAPSHOT"));
    assertThrows(IllegalArgumentException.class, ()->PathInfo.of("de.ldegen.maven.test","foo-bar","1.0","20201231.111111",42));
  }
}
