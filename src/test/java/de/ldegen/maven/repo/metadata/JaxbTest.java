package de.ldegen.maven.repo.metadata;

import static org.junit.jupiter.api.Assertions.*;

import java.io.StringWriter;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBException;

import org.junit.jupiter.api.Test;

import de.ldegen.maven.repo.metadata.Metadata.Plugins;

class JaxbTest {

  @Test
  void generalJaxbMarshallingWorksAsExpected() throws JAXBException {
    Metadata md = new Metadata();
    md.setGroupId("de.ldegen.maven.test");
    Plugins plugins = new Plugins();
    md.setPlugins(plugins);
    Plugin plugin = new Plugin();
    plugin.setArtifactId("perfect-maven-plugin");
    plugin.setPrefix("perfect");
    plugins.getPlugins().add(plugin);
    plugin = new Plugin();
    plugin.setArtifactId("maven-oldschool-plugin");
    plugin.setPrefix("oldschool");
    plugins.getPlugins().add(plugin);
    String output = toXml(md);
    String expected = """
      <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
      <metadata xmlns="http://maven.apache.org/METADATA/1.1.0">
          <groupId>de.ldegen.maven.test</groupId>
          <plugins>
              <plugin>
                  <prefix>perfect</prefix>
                  <artifactId>perfect-maven-plugin</artifactId>
              </plugin>
              <plugin>
                  <prefix>oldschool</prefix>
                  <artifactId>maven-oldschool-plugin</artifactId>
              </plugin>
          </plugins>
      </metadata>
      """;

    assertEquals(expected, output);
  }

  private String toXml(Metadata md) {
    StringWriter writer = new StringWriter();
    JAXB.marshal(md, writer);
    String output = writer.toString();
    return output;
  }
}
