package de.ldegen.maven.repo.metadata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;

import org.apache.commons.io.IOUtils;
import org.apache.maven.model.resolution.UnresolvableModelException;
import org.junit.jupiter.api.Test;

class PathBasedModelResolverTest {

  private final static Charset charset = Charset.forName("UTF-8");

  @Test
  public void looksUpPomsByConstructingAPath() throws IOException, UnresolvableModelException {
    InMemoryStorage storage = new InMemoryStorage();
    storage.put(Path.of("de", "ldegen", "maven", "test", "foo", "1.0.0", "foo-1.0.0.pom"), "foo".getBytes(charset));
    PathBasedModelResolver mr = new PathBasedModelResolver(storage);
    assertEquals("foo", resolve(mr, "de.ldegen.maven.test", "foo", "1.0.0"));
  }

  @Test
  public void failsEarlyInsteadOfProducingNullInput() throws Throwable {
    InMemoryStorage storage = new InMemoryStorage();
    storage.put(Path.of("de", "ldegen", "maven", "test", "foo", "1.0.0", "foo-1.0.0.pom"), "foo".getBytes(charset));
    PathBasedModelResolver mr = new PathBasedModelResolver(storage);

    assertThrows(UnresolvableModelException.class, () -> mr.resolveModel("de.ldegen.maven.test", "bar", "1.0.0"));
  }

  @Test
  public void resolvesVersionRanges() {
    InMemoryStorage storage = new InMemoryStorage();
    PathBasedModelResolver mr = new PathBasedModelResolver(storage);
    // ModelResolver.resolveModel(Parent) and ModelResolver.resolveModel(Dependeny)
    // must
    // both support version ranges. This is not implemented atm.
    // As with snapshot versions (see below), this would require existing metadata
    // for the
    // requested artifact. Should be possible in theory.

    fail("not implented");
  }

  

  /*
   * The referenced parent may very well be a snapshot version. In this case, we
   * need to lookup the maven-metadata.xml for this snapshot to resolve the most
   * recent pom. At first glance this might seem to create some chicken-and-egg
   * problem, but it actually does not. It only means that we have to recursively
   * generate metadata for that snapshot if it has not been created yet. It also
   * informs us that we must be able to do so with only the path/coordinates as
   * initial input, and of course our storage abstraction. It probably means that
   * our storage abstractions will have to provide us with some means of
   * scanning/traversing directories. We will worry about that later. For now, we
   * assume the metadata is already in the storage.
   * 
   * I had a look at org.apache.maven.repository.internal.DefaultVersionResolver
   * to figure out how snapshot artifacts are resolved in an actual maven build.
   * There, the metadata from (potentially) multiple repositories is merged on the
   * fly. It seems that the algorithm first looks at the <snapshotVersion>
   * elements. It will find the most "up-to-date" version for each artifact and/or
   * attached artifact. After merging it will return the resolved version for the
   * requested artifact or attached artifact. Only if the
   * <snapshotVersions>-Element is empty or missing will the <snapshot> element be
   * examined. Our situation is a bit different, we do not have multiple
   * repositories to worry about. But can we assume the <snapshotVersions> to be
   * unique for each "key"? Probably not, since we cannot be sure who or what
   * created the records in the first place.
   */
  @Test
  public void resolvesToLatestSnapshotVersion() throws Throwable {

    InMemoryStorage storage = new InMemoryStorage();
    PathBasedModelResolver mr = new PathBasedModelResolver(storage);
    storage.put(MetadataBuilder
        .at("de.ldegen.maven.test:foo:1.0.0-SNAPSHOT")
        .timestamp("yyyymmdd.hhmmss") //doesn't matter
        .buildNumber(9999) // doesn't matter
        .lastUpdated("yyyymmddhhmmss") // doesn't matter
        //Note: I think in real life their probably cannot be more than one version per (classifier,extension) pair. 
        //But it cannot hurt to be defensive here. Also note that our implementation
        //ignores the updated-Field and only compares the values.
        .snapshotVersion(sv -> sv.extension("pom").value("1.0.0-20160107.134234-35")) //simply sorting lexicographically by character won't do!
        .snapshotVersion(sv -> sv.extension("pom").value("1.0.0-20160107.134234-136"))
    );
    storage.put(Path.of("de","ldegen","maven","test","foo","1.0.0-SNAPSHOT","foo-1.0.0-20160107.134234-136.pom"),"pom content".getBytes(charset));
    assertEquals("pom content",resolve(mr,"de.ldegen.maven.test","foo","1.0.0-SNAPSHOT"));

  }
  
  @Test
  public void resolvesToLatestSnapshotVersionFallingBackToSnapshotIfSnapshotVersionsIsEmpty() throws Throwable {

    InMemoryStorage storage = new InMemoryStorage();
    PathBasedModelResolver mr = new PathBasedModelResolver(storage);
    storage.put(MetadataBuilder
        .at("de.ldegen.maven.test:foo:1.0.0-SNAPSHOT")
        .timestamp("20210524.164012") //does matter
        .buildNumber(42) // does matter
        .lastUpdated("yyyymmddhhmmss") // doesn't matter
        
    );
    storage.put(Path.of("de","ldegen","maven","test","foo","1.0.0-SNAPSHOT","foo-1.0.0-20210524.164012-42.pom"),"pom content".getBytes(charset));
    assertEquals("pom content",resolve(mr,"de.ldegen.maven.test","foo","1.0.0-SNAPSHOT"));

  }

  private static String resolve(PathBasedModelResolver mr, String groupId, String artifactId, String version)
      throws IOException, UnresolvableModelException {
    return IOUtils.toString(mr.resolveModel(groupId, artifactId, version).getInputStream(), charset);
  }

}
