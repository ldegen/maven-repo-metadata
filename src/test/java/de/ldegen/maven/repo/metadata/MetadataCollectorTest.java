
package de.ldegen.maven.repo.metadata;

import static org.junit.jupiter.api.Assertions.*;

import java.io.StringWriter;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.jupiter.api.Test;

import de.ldegen.maven.repo.metadata.Metadata.Plugins;
import de.ldegen.maven.repo.metadata.PomInfo.Packaging;

class MetadataCollectorTest {

  private String toXml(Metadata md) {
    StringWriter writer = new StringWriter();
    JAXB.marshal(md, writer);
    String output = writer.toString();
    return output;
  }
  public static PomInspector mockInspector(PomInfoBuilder ... poms) {
    HashMap<Path, PomInfoBuilder> lookup = new HashMap<>();
    for (PomInfoBuilder pom : poms) {
      lookup.put(pom.buildPath(), pom);
    }
    return (Path p)->Optional.ofNullable(lookup.get(p)).map(PomInfoBuilder::buildPomInfo).orElse(null);
  }
  @Test
  public void testGroupAndArtifactMetadata() {
    PomInfoBuilder pbA = PomInfoBuilder.of("de.ldegen.maven.test", "a-maven-plugin", "1.0").packaging(Packaging.plugin).name("Name A");
    PomInfoBuilder pbB = PomInfoBuilder.of("de.ldegen.maven.test", "maven-b-plugin", "1.0").packaging(Packaging.plugin).name("Name B");
    MetadataCollector collector = new MetadataCollector(mockInspector(pbA,pbB));
  
    MetadataBuilder expectedGroupMetadata = MetadataBuilder.at("de.ldegen.maven.test")
    .plugin(p->p.name("Name A").prefix("a").artifactId("a-maven-plugin"))
    .plugin(p->p.name("Name B").prefix("b").artifactId("maven-b-plugin"));
    
    
    MetadataBuilder expectedArtifactMetadataA = MetadataBuilder.at("de.ldegen.maven.test:a-maven-plugin")
        .latest("1.0").release("1.0").version("1.0");
    
    MetadataBuilder expectedArtifactMetadataB = MetadataBuilder.at("de.ldegen.maven.test:maven-b-plugin")
        .latest("1.0").release("1.0").version("1.0");
    
    
    Map<Path, Metadata> md = Stream.of(pbA, pbB).map(PomInfoBuilder::buildPath).collect(collector);

    assertTrue(md.containsKey(expectedGroupMetadata.buildPath()));
    assertEquals(expectedGroupMetadata.buildString(), toXml(md.get(expectedGroupMetadata.buildPath())));

    assertTrue(md.containsKey(expectedArtifactMetadataA.buildPath()));
    assertEquals(expectedArtifactMetadataA.buildString(), toXml(md.get(expectedArtifactMetadataA.buildPath())));

    assertTrue(md.containsKey(expectedArtifactMetadataB.buildPath()));
    assertEquals(expectedArtifactMetadataB.buildString(), toXml(md.get(expectedArtifactMetadataB.buildPath())));

    assertEquals(3, md.size());

  }
  
  @Test
  public void testSnapshotMetadata() {
    /*
     * snapshot metadata is tricky, because we need to know all the 
     * secondary artifacts deployed for that snapshot. Typically, plugins like the
     * maven-source-plugin "attach" secondary artifacts during the execution of their "mojo"
     * by calling MavenProject$addAttachedArtifact(...). So to determine the list of attached artifacts,
     * one would have to actually build the project the project, which we
     * certainly won't do. Even if that was feasible (which it is not!) it
     * would be quiet slow, certainly not something you want to do for each artifact when scanning your
     * repository.
     * 
     * But in a typical repository-manager-kind-of-situation, we might be able to fake it.
     * We basically could scan our local storage for paths that look like they belong to snapshot artifacts
     * for a given snapshot version
     */
  }
}
