package de.ldegen.maven.repo.metadata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.ldegen.maven.repo.metadata.PomInfo.Packaging;

class MetadataExtractorTest {

  private InMemoryStorage storage;
  private MetadataExtractor extractor;
  private final static String GROUP_ID = "de.ldegen.maven.test";

  @BeforeEach
  public void setup() throws PomInspectionException {

    this.storage = new InMemoryStorage();
    this.extractor = new MetadataExtractor(storage);
  }

  @Test
  public void test_explicitMapping() throws PomInspectionException, IOException {
    // this case combines a couple of interesting problems:
    // We have a plugin project that configures an explicit prefix mapping via the
    // maven-plugin-plugin.
    // To make things more interesting, the prefix is specified indirectly, using an
    // expression.
    // This is a quite common case, it is even recommended here
    // https://danielflower.github.io/2015/01/29/Generating-a-Maven-plugin-site-and-publishing-to-Github-Pages.html
    //
    // As long as the required properties are defined in within the same file, I
    // think this should be possible.
    // We could abuse parts of the maven core / plugin code to do the heavy lifting
    // for us.

    ModelBuilder input = ModelBuilder.of(GROUP_ID, "test", "1.0.0").packaging(Packaging.plugin).property("foo", "bar")
        .property("plugin.goal.prefix", "${foo}-baz")
        .plugin("maven-plugin-plugin", p -> p.config("goalPrefix", "${plugin.goal.prefix}"));

    PomInfo pom = extractor.examine(input.buildString());
    assertEquals("de.ldegen.maven.test", pom.getGroupId());
    assertEquals("test", pom.getArtifactId());
    assertEquals(Packaging.plugin, pom.getPackaging());
    assertEquals("test", pom.getName());
    assertEquals("1.0.0", pom.getVersion());
    assertEquals("bar-baz", pom.getGoalPrefix());
  }

  @Test
  public void test_implicitMapping_normalPattern() throws PomInspectionException, IOException {
    ModelBuilder input = ModelBuilder.of(GROUP_ID, "maven-foo-plugin", "1.0.0").packaging(Packaging.plugin);
    PomInfo pom = extractor.examine(input.buildString());
    assertEquals("de.ldegen.maven.test", pom.getGroupId());
    assertEquals("maven-foo-plugin", pom.getArtifactId());
    assertEquals(Packaging.plugin, pom.getPackaging());
    assertEquals("maven-foo-plugin", pom.getName());
    assertEquals("1.0.0", pom.getVersion());
    assertEquals("foo", pom.getGoalPrefix());
  }

  @Test
  public void test_implicitMapping_reservedPattern() throws PomInspectionException, IOException {
    ModelBuilder input = ModelBuilder.of(GROUP_ID, "foo-maven-plugin", "1.0.0").packaging(Packaging.plugin);
    PomInfo pom = extractor.examine(input.buildString());
    assertEquals("de.ldegen.maven.test", pom.getGroupId());
    assertEquals("foo-maven-plugin", pom.getArtifactId());
    assertEquals(Packaging.plugin, pom.getPackaging());
    assertEquals("foo-maven-plugin", pom.getName());
    assertEquals("1.0.0", pom.getVersion());
    assertEquals("foo", pom.getGoalPrefix());

  }

  @Test
  public void test_submodule_with_missing_parent() {
    ModelBuilder input = ModelBuilder.of(GROUP_ID, "foo", "1.0.0").parent("my-parent");

    // this cannot work, because our pom references a parent pom that cannot be
    // resolved in our storage. I do think we *want* this to fail. We
    // *could* in theory provide a stub for the parent pom, so *some* model would be
    // generated. But it may not be the correct one, and I think it is more
    // dangerous to have incorrect meta-data then to not have them at all (and know
    // it).
    assertThrows(PomInspectionException.class, () -> {
      extractor.examine(input.buildString());
    });
  }

  @Test
  public void test_submodule() throws PomInspectionException, IOException {
    // provide a trivial parent pom, because building the model will
    // fail if a referenced parent cannot be resolved.
    storage.put(ModelBuilder.of(GROUP_ID, "my-parent", "1.0.0").packaging(Packaging.pom));

    ModelBuilder childInput = ModelBuilder.of(GROUP_ID, "foo", "1.0.0").parent("my-parent").name("Maven Repo Metadata");
    PomInfo pom = extractor.examine(childInput.buildString());
    assertEquals("de.ldegen.maven.test", pom.getGroupId());
    assertEquals("foo", pom.getArtifactId());
    assertEquals(Packaging.jar, pom.getPackaging());
    assertEquals("Maven Repo Metadata", pom.getName());
    assertEquals("1.0.0", pom.getVersion());
    // note: this is *not* a plugin, so we are getting no prefix
    assertNull(pom.getGoalPrefix());
  }

  @Test
  public void test_submodule_propsFromParent() throws Throwable {
    // We expect this to be more difficult. The problem at hands here is that in our
    // submodule we refer to
    // properties defined in the parent module. Unfortunately this is too common a
    // case to simply ignore it.
    // Our solution does probably not work 100%, but it should catch most of the
    // "usual" cases.

    ModelBuilder parent = ModelBuilder.of(GROUP_ID, "my-parent", "1.0.0").packaging(Packaging.pom).property("foo", "bar");
    ModelBuilder submodule = ModelBuilder.of(GROUP_ID, "foo", "1.0.0").packaging(Packaging.plugin).parent("my-parent")
        .property("plugin.goal.prefix", "${foo}-baz")
        .plugin("maven-plugin-plugin", p->p.config("goalPrefix", "${plugin.goal.prefix}"));
    storage.put(parent);
    storage.put(submodule);
    


    PomInfo pom = extractor.examine(submodule.buildPath());
    assertEquals(GROUP_ID, pom.getGroupId());
    assertEquals("foo", pom.getArtifactId());
    assertEquals(Packaging.plugin, pom.getPackaging());
    assertEquals("1.0.0", pom.getVersion());
    assertEquals("bar-baz", pom.getGoalPrefix());
  }

}
