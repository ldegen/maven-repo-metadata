package de.ldegen.maven.repo.metadata;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.file.Path;

import org.junit.jupiter.api.Test;
import static de.ldegen.maven.repo.metadata.MetadataUtils.*;
class MetadataUtilsTest {

  @Test
  void guessPathForSnapshotMetadata() {
    Path actual = metadataPath(MetadataBuilder.at("my.group.id:artifact-id:1.2.3-SNAPSHOT").build());
    Path expected = Path.of("my","group","id","artifact-id","1.2.3-SNAPSHOT","maven-metadata.xml");
    assertEquals(expected,actual);
  }
  
  @Test
  void guessPathForArtifactMetadata() {
    Path actual = metadataPath(MetadataBuilder.at("my.group.id:artifact-id").build());
    Path expected = Path.of("my","group","id","artifact-id","maven-metadata.xml");
    assertEquals(expected,actual);
  }
  @Test
  void guessPathForGroupMetadata() {
    Path actual = metadataPath(MetadataBuilder.at("my.group.id").build());
    Path expected = Path.of("my","group","id","maven-metadata.xml");
    assertEquals(expected,actual);
  }

  @Test
  void compareSnapshotVersionsByValue() {
    SnapshotVersion a = new SnapshotVersionBuilder().value("1.0.0-20160107.134234-136").build();
    SnapshotVersion b = new SnapshotVersionBuilder().value("1.0.0-20160107.134234-35").build();
    assertTrue(compareSnapshotVersions(a, b) > 0);
  }
}
