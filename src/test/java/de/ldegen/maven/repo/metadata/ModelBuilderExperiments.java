package de.ldegen.maven.repo.metadata;

import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.maven.model.Model;
import org.apache.maven.model.building.DefaultModelBuilder;
import org.apache.maven.model.building.DefaultModelBuilderFactory;
import org.apache.maven.model.building.DefaultModelBuildingRequest;
import org.apache.maven.model.building.ModelBuildingResult;
import org.apache.maven.model.building.ModelCache;
import org.apache.maven.model.building.StringModelSource;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.apache.maven.model.resolution.UnresolvableModelException;
import org.apache.maven.model.resolution.WorkspaceModelResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

public class ModelBuilderExperiments extends TestCase {
  private final class UselessCache implements ModelCache {
    HashMap<String, Object> map = new HashMap<>();

    @Override
    public void put(String groupId, String artifactId, String version, String tag, Object data) {
      String key = key(groupId, artifactId, version);
      map.put(key, data);
    }

    private String key(String groupId, String artifactId, String version) {
      return Stream.of(groupId, artifactId, version).collect(Collectors.joining(":"));
    }

    @Override
    public Object get(String groupId, String artifactId, String version, String tag) {
      String key = key(groupId, artifactId, version);
      Object data = map.get(key);
      return data;
    }
  }

  private final class MyModelResolver implements WorkspaceModelResolver {
    HashMap<String, Model> rawModels = new HashMap<>();
    HashMap<String, Model> effectiveModels = new HashMap<>();

    private String key(String groupId, String artifactId, String version) {
      return Stream.of(groupId, artifactId, version).collect(Collectors.joining(":"));
    }

    @Override
    public Model resolveRawModel(String groupId, String artifactId, String versionConstraint)
        throws UnresolvableModelException {
      return rawModels.get(key(groupId, artifactId, versionConstraint));
    }

    @Override
    public Model resolveEffectiveModel(String groupId, String artifactId, String versionConstraint)
        throws UnresolvableModelException {
      return effectiveModels.get(key(groupId, artifactId, versionConstraint));
    }

    public void put(ModelBuildingResult result) {
      Model raw = result.getRawModel();
      raw.setPomFile(new File("/i/do/not/exist"));
      Model effective = result.getEffectiveModel();
      String key = key(raw.getGroupId(), raw.getArtifactId(), raw.getVersion());
      effectiveModels.put(key, effective);
      rawModels.put(key, raw);
    }
  }

  String parent = """
    <project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
          http://maven.apache.org/xsd/maven-4.0.0.xsd">
      <modelVersion>4.0.0</modelVersion>
      <groupId>de.ldegen.maven.test</groupId>
      <artifactId>my-parent</artifactId>
      <packaging>pom</packaging>
      <version>1.0.0</version>
      <name>Maven Repo Metadata</name>
      <properties>
        <foo>bar</foo>
      </properties>
    </project>
    """;
  String submodule = """
    <project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
          http://maven.apache.org/xsd/maven-4.0.0.xsd">
      <modelVersion>4.0.0</modelVersion>
      <parent>
        <groupId>de.ldegen.maven.test</groupId>
        <version>1.0.0</version>
        <artifactId>my-parent</artifactId>
      </parent>
      <artifactId>foo</artifactId>
      <packaging>maven-plugin</packaging>
      <name>Maven Repo Metadata</name>
      <properties>
        <plugin.goal.prefix>${foo}-baz</plugin.goal.prefix>
      </properties>
      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-plugin-plugin</artifactId>
            <version>3.4</version>
            <configuration>
              <goalPrefix>${plugin.goal.prefix}</goalPrefix>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </project>
    """;
  String dependent="""
    <project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
          http://maven.apache.org/xsd/maven-4.0.0.xsd">
      <modelVersion>4.0.0</modelVersion>
      <groupId>de.ldegen.maven.test</groupId>
      <version>1.0.0</version>
      <artifactId>dependent</artifactId>
      <dependencies>
        <dependency>
          <groupId>de.ldegen.maven.test</groupId>
          <version>[1.0.0,1.5.9]</version>
          <artifactId>dependency</artifactId>
        </dependency>
      </dependencies>
    </project>
    """;
  private PathBasedModelResolver modelResolver;

  @BeforeEach
  public void setup() {
    InMemoryStorage storage = new InMemoryStorage();
    storage.put(Path.of("de","ldegen","maven","test","dependent","1.0.0","dependent-1.0.0.pom"), dependent.getBytes());
    storage.put(Path.of("de","ldegen","maven","test","foo","1.0.0","foo-1.0.0.pom"), submodule.getBytes());
    storage.put(Path.of("de","ldegen","maven","test","my-parent","1.0.0","my-parent-1.0.0.pom"), parent.getBytes());
    this.modelResolver = new PathBasedModelResolver(storage);
  }

  @Test
  public void resolvingParentPomUsingWorkspaceModelProvider() throws Throwable {
    StringModelSource parentSource = new StringModelSource(parent);
    StringModelSource submoduleSource = new StringModelSource(submodule);

    DefaultModelBuilder dmb = new DefaultModelBuilderFactory().newInstance();

    DefaultModelBuildingRequest moduleRequest = new DefaultModelBuildingRequest();
    DefaultModelBuildingRequest parentRequest = new DefaultModelBuildingRequest();
    ModelCache cache = new UselessCache();

    MyModelResolver resolver = new MyModelResolver();
    moduleRequest.setModelCache(cache);
    parentRequest.setModelCache(cache);
    moduleRequest.setWorkspaceModelResolver(resolver);
    parentRequest.setWorkspaceModelResolver(resolver);
    moduleRequest.setModelSource(submoduleSource);
    parentRequest.setModelSource(parentSource);

    ModelBuildingResult parentResult = dmb.build(parentRequest);
    resolver.put(parentResult);
    ModelBuildingResult moduleResult = dmb.build(moduleRequest);
    resolver.put(moduleResult);
    Model effectiveModel = moduleResult.getEffectiveModel();
    assertEquals("bar-baz",effectiveModel.getProperties().get("plugin.goal.prefix"));
  }

  @Test
  public void doPathsWorkAsIThinkTheyShould() {
    Path p = Path.of("foo", "bar", "baz", "..", "..", "bang", ".");
    assertEquals(Path.of("foo", "bang"), p.normalize());
  }

  @Test
  public void resolvingParentPomByAssumingSomeKindOfStorageAbstraction() throws Throwable {
    DefaultModelBuilder dmb = new DefaultModelBuilderFactory().newInstance();

    DefaultModelBuildingRequest moduleRequest = new DefaultModelBuildingRequest();

    moduleRequest.setModelResolver(modelResolver);
    moduleRequest.setModelSource(modelResolver.resolveModel("de.ldegen.maven.test", "foo", "1.0.0"));

    ModelBuildingResult moduleResult = dmb.build(moduleRequest);
    Model effectiveModel = moduleResult.getEffectiveModel();
    assertEquals("bar-baz",effectiveModel.getProperties().get("plugin.goal.prefix"));
  }
  
  @Test
  public void effectiveModelDoesNotRequireResolvedDependencies() throws Throwable {
    
    // just making sure that building the effective model does *NOT* involve dependency
    // version mediation or similar since this can get complex really quickly.
    DefaultModelBuilder dmb = new DefaultModelBuilderFactory().newInstance();

    DefaultModelBuildingRequest moduleRequest = new DefaultModelBuildingRequest();

    moduleRequest.setModelResolver(modelResolver);
    moduleRequest.setModelSource(modelResolver.resolveModel("de.ldegen.maven.test", "dependent", "1.0.0"));

    ModelBuildingResult moduleResult = dmb.build(moduleRequest);
    
    //if we got this far without an exception, that is actually a good sign: 
    //I deliberately added a dependency to an artifact "de.ldegen.maven.test:dependency:1.0.0" that does not
    //exist in our storage. From my understanding, this should *not* be a problem at this stage, since
    //only parent references need to be resolved for the effective model, not dependencies.
    Model effectiveModel = moduleResult.getEffectiveModel();
    //Also, I used a version range in the dependency. I think the ModelBuilder should not touch it at all,
    //So in the effective model, there should still be the unresolved range expression
    assertEquals("[1.0.0,1.5.9]",effectiveModel.getDependencies().get(0).getVersion());
    
    // so basically all we need to care about are parent references. Which can be tricky enough, since
    // they, too, are subject to version mediation.
     
  }
}
