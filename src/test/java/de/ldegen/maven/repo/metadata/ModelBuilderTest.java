package de.ldegen.maven.repo.metadata;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.apache.maven.model.Model;
import org.junit.jupiter.api.Test;

import de.ldegen.maven.repo.metadata.PomInfo.Packaging;

class ModelBuilderTest {

  @Test
  void test() throws IOException {
    String expected = """
      <?xml version="1.0" encoding="UTF-8"?>
      <project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <modelVersion>4.0.0</modelVersion>
        <parent>
          <groupId>de.ldegen.maven.test</groupId>
          <artifactId>my-parent</artifactId>
          <version>1.0.0</version>
        </parent>
        <artifactId>test</artifactId>
        <packaging>maven-plugin</packaging>
        <name>Maven Repo Metadata</name>
        <properties>
          <foo>bar</foo>
          <plugin.goal.prefix>${foo}-baz</plugin.goal.prefix>
        </properties>
        <build>
          <plugins>
            <plugin>
              <artifactId>maven-plugin-plugin</artifactId>
              <version>3.4</version>
              <configuration>
                <goalPrefix>${plugin.goal.prefix}</goalPrefix>
              </configuration>
            </plugin>
          </plugins>
        </build>
      </project>
      """;
    String actual = ModelBuilder.of("de.ldegen.maven.test", "test", "1.0.0").packaging(Packaging.plugin)
        .name("Maven Repo Metadata").property("foo", "bar").property("plugin.goal.prefix", "${foo}-baz")
        .parent("my-parent")
        .plugin(
            "org.apache.maven.plugins", "maven-plugin-plugin", "3.4",
            p -> p.config("goalPrefix", "${plugin.goal.prefix}")
        ).buildString();
    assertEquals(expected, actual);
  }

  @Test
  public void testDefaultPluginGroupId() {
    Model m = ModelBuilder.of("de.ldegen.maven.test", "test", "1.0.0")
        .plugin("maven-plugin-plugin", p -> p.config("goalPrefix", "${plugin.goal.prefix}")).buildModel();
    assertEquals("org.apache.maven.plugins", m.getBuild().getPlugins().get(0).getGroupId());
  }

}
