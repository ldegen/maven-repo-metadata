package de.ldegen.maven.repo.metadata;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.Parent;
import org.apache.maven.model.Repository;
import org.apache.maven.model.building.ModelSource;
import org.apache.maven.model.resolution.InvalidRepositoryException;
import org.apache.maven.model.resolution.ModelResolver;
import org.apache.maven.model.resolution.UnresolvableModelException;
/**
 * @author lukas
 *
 * Base class for simplified state-less resolvers.
 * Note that {@link StatelessModelResolver#addRepository(Repository)} and friends do nothing and are final,
 * as is {@link StatelessModelResolver#newCopy()}. The latter simply returns self, which should be safe
 * with the former being no-ops.
 */
public abstract class StatelessModelResolver implements ModelResolver {

  @Override
  public abstract ModelSource 
  resolveModel(String groupId, String artifactId, String version) throws UnresolvableModelException;

  @Override
  public final ModelSource resolveModel(Parent parent) throws UnresolvableModelException {
    return resolveModel(parent.getGroupId(), parent.getArtifactId(), parent.getVersion());
  }

  @Override
  public final ModelSource resolveModel(Dependency dependency) throws UnresolvableModelException {
    return resolveModel(dependency.getGroupId(),dependency.getArtifactId(),dependency.getVersion());
  }

  @Override
  public final void addRepository(Repository repository) throws InvalidRepositoryException {
    addRepository(repository, false);
  }

  @Override
  public final void addRepository(Repository repository, boolean replace) throws InvalidRepositoryException {
  }

  @Override
  public final ModelResolver newCopy() {
    return this;
  }

}
