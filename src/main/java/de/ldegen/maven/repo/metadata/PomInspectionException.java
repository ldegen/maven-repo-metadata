package de.ldegen.maven.repo.metadata;

public class PomInspectionException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = -7414214473165764173L;

  public PomInspectionException(String msg, Throwable e) {
    super(msg,e);
  }

}
