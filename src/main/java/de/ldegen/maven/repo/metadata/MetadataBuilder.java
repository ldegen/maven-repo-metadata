package de.ldegen.maven.repo.metadata;
import static de.ldegen.maven.repo.metadata.MetadataUtils.*;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.UnaryOperator;

import javax.xml.bind.JAXB;


/**
 * test data builder for {@link Metadata} objects.
 * This is not intended for production use.
 * @author lukas
 *
 */

public class MetadataBuilder extends Builder<Metadata,MetadataBuilder>{
  public static class PluginMetadataBuilder extends Builder<Plugin,PluginMetadataBuilder>{

    Plugin p;
    public PluginMetadataBuilder() {
      this(new Plugin());
    }
    public PluginMetadataBuilder(Plugin plugin) {
      p = plugin;
    }
    @Override
    protected Plugin get() {
      return p;
    }

    public Plugin build() {
      return p;
    }
    @Override
    protected PluginMetadataBuilder unit(Plugin v) {
      return new PluginMetadataBuilder(v);
    }

    @Override
    protected Plugin copy(Plugin orig) {
      return deepCopy(orig);
    }
    
    public PluginMetadataBuilder name(String name) {
      return modify(cpy->cpy.setName(name));
    }
    public PluginMetadataBuilder artifactId(String aid) {
      return modify(cpy->cpy.setArtifactId(aid));
    }
    public PluginMetadataBuilder prefix(String prefix) {
      return modify(cpy->cpy.setPrefix(prefix));
    }

  }
  private final Metadata md;
  
  private MetadataBuilder(Metadata md) {
    this.md = md;
  }

  public static MetadataBuilder at(String spec, String ...more) {
    ArrayList<String> coordinates = new ArrayList<>();
    coordinates.addAll(Arrays.asList(spec.split(":")));
    coordinates.addAll(Arrays.asList(more));
    Metadata md = new Metadata();
    md.setModelVersion("1.1.0");
    md.setGroupId(coordinates.get(0));
    if(coordinates.size()>1) {
      md.setArtifactId(coordinates.get(1));
    }
    if(coordinates.size()>2) {
      md.setVersion(coordinates.get(2));
    }
    if(coordinates.size()>3) {
      throw new IllegalArgumentException("do not understand more than 3 coordinates:"+coordinates);
    }
    return new MetadataBuilder(md);
  }

  public MetadataBuilder timestamp(String ts) {
    return modify(cp->snapshot(cp).setTimestamp(ts));
  }

  public MetadataBuilder buildNumber(Integer bn) {
    return modify(cp->snapshot(cp).setBuildNumber(bn));
  }
  public MetadataBuilder localCopy(Boolean lc) {
    return modify(cp->snapshot(cp).setLocalCopy(lc));
  }
  public MetadataBuilder snapshotVersion(UnaryOperator<SnapshotVersionBuilder> op) {
    return modify((cp)->{
      snapshotVersions(cp).add(op.apply(new SnapshotVersionBuilder()).build());
    });
  }
  public MetadataBuilder plugin(UnaryOperator<PluginMetadataBuilder> op) {
    return modify((cp)->{
      plugins(cp).add(op.apply(new PluginMetadataBuilder()).build());
    });
  }
  public MetadataBuilder lastUpdated(String string) {
    return modify(cp->versioning(cp).setLastUpdated(string));
  }
  public MetadataBuilder latest(String string) {
    return modify(cp->versioning(cp).setLatest(string));
  }
  public MetadataBuilder release(String string) {
    return modify(cp->versioning(cp).setRelease(string));
  }
  public MetadataBuilder version(String string) {
    return modify(cp->versions(cp).add(string));
  }

  public byte[] buildByteArray() {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    JAXB.marshal(build(), os);
    return os.toByteArray();
  }

  public Metadata build() {
    return md;
  }

  public String buildString() {
    StringWriter w = new StringWriter();
    JAXB.marshal(build(), w);
    return w.toString();
  }
  
  public Path buildPath() {
    return metadataPath(build());
  }

  @Override
  protected Metadata get() {
    return md;
  }

  @Override
  protected MetadataBuilder unit(Metadata v) {
    return new MetadataBuilder(v);
  }

  @Override
  protected Metadata copy(Metadata orig) {
    return deepCopy(orig);
  }

  
  
  
}
