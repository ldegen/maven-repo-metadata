package de.ldegen.maven.repo.metadata;

import java.nio.file.Path;

@FunctionalInterface
public interface PomInspector {

  PomInfo examine(Path path) throws PomInspectionException;

}