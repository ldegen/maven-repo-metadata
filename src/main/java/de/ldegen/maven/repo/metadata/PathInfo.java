package de.ldegen.maven.repo.metadata;

import java.nio.file.Path;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * information obtained by looking at a leaf path
 * i.e. a path from the repository root down to an artifact file.
 * 
 * <br>
 * In some cases you will have to actually look inside a POM. For this, you would need a
 * {@link PomInspector} that will create a {@link PomInfo} for you.
 * @author lukas
 *
 */
public class PathInfo {

  private static final String _SNAPSHOT = "-SNAPSHOT";
  public final String groupId;
  public final String artifactId;
  public final String version;
  public final String fileName;
 

  public final String targetVersion;
  public final boolean snapshot;
  public final String subversion;
  public final String extension;
  public final Integer buildNumber;
  public final String timestamp;
  public final String classifier;

  
  private PathInfo(
      String groupId, String artifactId, String version, String extension, Integer buildNumber, String timestamp,
      String classifier
  ) {
    
    this.groupId = groupId;
    this.artifactId = artifactId;
    this.version = version;
    this.extension = extension;
    this.buildNumber = buildNumber;
    this.timestamp = timestamp;
    this.classifier = classifier;
    this.snapshot = version.endsWith(_SNAPSHOT);
    if(snapshot) {
      if(timestamp==null||buildNumber==null) {
        throw new IllegalArgumentException("snapshot version needs timestamp and buildNumber");
      }
    }else {
      if(timestamp!=null||buildNumber!=null) {
        throw new IllegalArgumentException("no timestamp or build number allowed for snapshot version");
      }
    }
    this.targetVersion=snapshot ? version.substring(0, version.length()-_SNAPSHOT.length()) : version;
    this.subversion=snapshot ? targetVersion+"-"+timestamp+"-"+buildNumber : version;
    this.fileName=artifactId+"-"+subversion+(classifier==null?"":"-"+classifier)+"."+extension;
    
  }
  public static class PathInfoOrError{
    final public PathInfo pathInfo;
    final public IllegalArgumentException error;
    public PathInfoOrError(IllegalArgumentException error) {
      this.error = error;
      this.pathInfo=null;
    }
    public PathInfoOrError(PathInfo pathInfo) {
      this.pathInfo = pathInfo;
      this.error = null;
    }
    public static PathInfoOrError of(Path p) {
      if (p.getNameCount() < 4) {
        return new PathInfoOrError(new IllegalArgumentException("Path is to short: " + p));
      }
      final int groupLength = p.getNameCount() - 3;
      final String groupId = StreamSupport.stream(Spliterators.spliteratorUnknownSize(p.iterator(), Spliterator.ORDERED), false)
          .limit(groupLength).map(Path::toString).collect(Collectors.joining("."));
      final String artifactId=p.getName(groupLength).toString();
      final String version=p.getName(groupLength+1).toString();
      final String fileName=p.getName(groupLength+2).toString();
      final boolean snapshot = version.endsWith(_SNAPSHOT);
      final String targetVersion=snapshot ? version.substring(0, version.length()-_SNAPSHOT.length()) : version;
      final String knownPartOfFileName = artifactId+"-"+targetVersion;
      if(!fileName.startsWith(knownPartOfFileName)) {
        return new PathInfoOrError( new IllegalArgumentException("file name must start with "+knownPartOfFileName));
      }
      final String timestamp, classifier,extension;
      final Integer buildNumber;
      final Pattern pat;
      final Matcher matcher;
      if(snapshot) {
        pat = Pattern.compile("^-([0-9]{8}\\.[0-9]{6})-([0-9]+)(-([^.]+))?\\.([^.]+)$");
        matcher = pat.matcher(fileName.substring(knownPartOfFileName.length()));
        if(!matcher.matches()) {
          return new PathInfoOrError(new IllegalArgumentException("bad file name: "+fileName));
        }
        timestamp=matcher.group(1);
        buildNumber=Integer.valueOf(matcher.group(2), 10);
        classifier=matcher.group(4);
        extension=matcher.group(5);
      } else {
        pat = Pattern.compile("^(-([^.]+))?\\.([^.]+)$");
        matcher = pat.matcher(fileName.substring(knownPartOfFileName.length()));
        if(!matcher.matches()) {
          return new PathInfoOrError(new IllegalArgumentException("bad file name: "+fileName));
        }
        timestamp=null;
        buildNumber=null;
        classifier=matcher.group(2);
        extension=matcher.group(3);
      }
      
      return new PathInfoOrError(new PathInfo(groupId, artifactId, version, extension, buildNumber, timestamp, classifier));
    }
  }
 
  public static PathInfo of(Path path) {
    return PathInfoOrError.of(path).pathInfo;
  }
  
  public String getGroupId() {
    return groupId;
  }

  public String getArtifactId() {
    return artifactId;
  }

  public String getVersion() {
    return version;
  }

  public String getSubversion() {
    return subversion;
  }

  public String getExtension() {
    return extension;
  }

  public String getTargetVersion() {
    return targetVersion;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public Integer getBuildNumber() {
    return buildNumber;
  }

  public String getClassifier() {
    return classifier;
  }

  public Path getGroupPath() {
    return Path.of("",groupId.split("\\."));
  }
  
  public Path getPath() {
    return getVersionPath().resolve(Path.of(fileName));
  }

  public Path getVersionPath() {
    return getArtifactPath().resolve(Path.of(version));
  }

  public Path getArtifactPath() {
    return getGroupPath().resolve(Path.of(artifactId));
  }

  public static PathInfo of(String groupId, String artifactId, String version) {
    return new PathInfo(groupId,artifactId,version,"jar",null,null,null);
  }

  public PathInfo withClassifier(String classifier) {
    return new PathInfo(groupId, artifactId, version, extension, buildNumber, timestamp, classifier);
  }

  public PathInfo withExtension(String extension) {
    return new PathInfo(groupId, artifactId, version, extension, buildNumber, timestamp, classifier);
    
  }

  public static PathInfo of(String groupId, String artifactId, String version, String timestamp,Integer buildNumber) {
    return new PathInfo(groupId, artifactId, version, "jar", buildNumber, timestamp, null);
  }

}
