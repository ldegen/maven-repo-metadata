package de.ldegen.maven.repo.metadata;

import static de.ldegen.maven.repo.metadata.MetadataUtils.deepCopy;


public class SnapshotVersionBuilder extends Builder<SnapshotVersion,SnapshotVersionBuilder>{
  private final SnapshotVersion sv;

  public SnapshotVersionBuilder(SnapshotVersion sv) {
    this.sv = sv;
  }

  public SnapshotVersionBuilder() {
    this(new SnapshotVersion());
  }

  public SnapshotVersionBuilder classifier(String cl) {
    return modify((c)->c.setClassifier(cl));
  }
  public SnapshotVersionBuilder value(String v) {
    return modify((c)->c.setValue(v));
  }
  public SnapshotVersionBuilder extension(String ext) {
    return modify((c)->c.setExtension(ext));
  }
  public SnapshotVersionBuilder updated(String upd) {
    return modify((c)->c.setUpdated(upd));
  }

  @Override
  protected SnapshotVersion get() {
    return sv;
  }

  @Override
  protected SnapshotVersionBuilder unit(SnapshotVersion v) {
    return new SnapshotVersionBuilder(v);
  }

  @Override
  protected SnapshotVersion copy(SnapshotVersion orig) {
    return deepCopy(orig);
  }

  public SnapshotVersion build() {
    return sv;
  }
}