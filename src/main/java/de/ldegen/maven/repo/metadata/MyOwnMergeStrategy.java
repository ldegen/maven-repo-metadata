package de.ldegen.maven.repo.metadata;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.ldegen.maven.repo.metadata.Metadata.Plugins;
import de.ldegen.maven.repo.metadata.Versioning.Versions;



/**
 * 
 *@deprecated This implementation is currently incomplete
 *
 */
public class MyOwnMergeStrategy implements MetadataMergeStrategy {
  @Override
  public Metadata merge(Metadata a, Metadata b) {
    if (a == null) {
      return b;
    }
    if (b == null) {
      return a;
    }

    // artifact id should either be set for both records, or it should be absent
    // from both.
    if (
      (a.getArtifactId() != null && b.getArtifactId() == null || a.getArtifactId() == null && b.getArtifactId() != null)
    ) {
      throw new IllegalArgumentException("artifactId must either be present in both records or in none");
    }
    // same for the version (used for snapshots only)
    if ((a.getVersion() != null && b.getVersion() == null || a.getVersion() == null && b.getVersion() != null)) {
      throw new IllegalArgumentException("version must either be present in both records or in none");
    }

    // groupId must always be present
    if (a.getGroupId() == null || b.getGroupId() == null) {
      throw new IllegalArgumentException("groupId must be present in both records");
    }

    // if two metadata files are given for the same (logical) path,
    // they should refer to the same group or artifact.

    if (!a.getGroupId().equals(b.getGroupId())) {
      throw new IllegalArgumentException("cannot merge metadata records for different groupIds");
    }

    // if present, the artifactIds must match as well.
    if (a.getArtifactId() != null && !a.getArtifactId().equals(b.getArtifactId())) {
      throw new IllegalArgumentException("cannot merge metadata records for different artifacts");
    }
    // if a version is present, so should be the artifactId
    if (a.getVersion() != null && a.getArtifactId() == null) {
      throw new IllegalArgumentException("records cannot have a version without an artifactId");
    }
    // if present, the versions must match as well.
    if (a.getVersion() != null && !a.getVersion().equals(b.getVersion())) {
      throw new IllegalArgumentException("cannot merge metadata records for different versions");
    }

    Metadata c = new Metadata();
    c.setArtifactId(a.getArtifactId());
    c.setGroupId(a.getGroupId());
    c.setModelVersion("1.1.0"); // TODO: that's the only version I know of?
    c.setVersion(a.getVersion());
    c.setVersioning(merge(a.getVersioning(), b.getVersioning()));
    c.setPlugins(merge(a.getPlugins(), b.getPlugins()));
    return c;
  }

  private Plugins merge(Plugins plugins, Plugins plugins2) {
    // TODO Auto-generated method stub
    return null;
  }

  private Versioning merge(Versioning a, Versioning b) {
    Stream<SnapshotVersion> ass = Optional.ofNullable(a.getSnapshotVersions()).map(vs->vs.getSnapshotVersions().stream()).orElse(Stream.empty());
    Stream<SnapshotVersion> bss = Optional.ofNullable(b.getSnapshotVersions()).map(vs->vs.getSnapshotVersions().stream()).orElse(Stream.empty());
    
    Stream<String> avs = Optional.ofNullable(a.getVersions()).map(vs->vs.getVersions().stream()).orElse(Stream.empty());
    Stream<String> bvs = Optional.ofNullable(b.getVersions()).map(vs->vs.getVersions().stream()).orElse(Stream.empty());
    
    List<String> mergedVersions = Stream.concat(avs, bvs).map(ComparableVersion::new).sorted().distinct()
        .map(ComparableVersion::toString).collect(Collectors.toList());
    
    Versioning c = new Versioning();
    c.setVersions(new Versions());
    c.getVersions().getVersions().addAll(mergedVersions);
    return c;
  }
}
