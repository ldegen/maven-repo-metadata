package de.ldegen.maven.repo.metadata;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import org.apache.maven.model.resolution.ModelResolver;
import org.apache.maven.model.resolution.UnresolvableModelException;

import de.ldegen.maven.repo.metadata.PathInfo.PathInfoOrError;

public class MetadataCollector implements Collector<Path, Map<Path, Metadata>, Map<Path, Metadata>> {
  private final MetadataMergeStrategy mergeStrategy;
  private final static Set<Characteristics> charactaristics = Set.of(Characteristics.IDENTITY_FINISH, Characteristics.UNORDERED);
  private final PomInspector pomInspector;
  public MetadataCollector(PomInspector pomInspector) {
    this(new HybridMergeStrategy(),pomInspector);
  }
  public MetadataCollector(MetadataMergeStrategy mergeStrategy,PomInspector pomInspector) {
    this.mergeStrategy = mergeStrategy;
    this.pomInspector = pomInspector;
  }

  @Override
  public Supplier<Map<Path, Metadata>> supplier() {
    return () -> new HashMap<>();
  }

  @Override
  public BiConsumer<Map<Path, Metadata>, Path> accumulator() {
    return this::accumulate;
  }

  @Override
  public BinaryOperator<Map<Path, Metadata>> combiner() {
    return this::merge;
  }

  @Override
  public Function<Map<Path, Metadata>, Map<Path, Metadata>> finisher() {
    return Function.identity();
  }

  @Override
  public Set<Characteristics> characteristics() {
    return charactaristics;
  }

  public void accumulate(Map<Path, Metadata> r, Path path) {
    PathInfoOrError pioe = PathInfoOrError.of(path);
    if(pioe.error!=null) {
      throw pioe.error;
    }
    accumulate(r,pioe.pathInfo);
  }
  
  public void accumulate(Map<Path, Metadata> r, PathInfo pathInfo) {
    Path metadataFilename = Path.of("maven-metadata.xml");
    
    mergeNewEntry(r, pathInfo.getArtifactPath().resolve(metadataFilename), constructArtifactMetadata(pathInfo));
    if(pathInfo.snapshot) {
      mergeNewEntry(r,pathInfo.getVersionPath().resolve(metadataFilename), constructSnapshotMetadata(pathInfo));
    }
    if("pom".equals(pathInfo.getExtension())) {
      mergeNewEntry(r, pathInfo.getGroupPath().resolve(metadataFilename), constructGroupMetadata(pathInfo));
    }
    
  }
  
  private Metadata constructSnapshotMetadata(PathInfo pathInfo) {
    // TODO Auto-generated method stub
    return null;
  }
  private void mergeNewEntry(Map<Path, Metadata> r, Path path, Metadata newEntry) {
    if(path==null) {
      throw new IllegalArgumentException();
    }
    if(newEntry!=null) {
      r.merge(path, newEntry, mergeStrategy::merge);
    }
  }
  private Metadata constructGroupMetadata(PathInfo pathInfo) {
    
    return constructGroupMetadata(examinePom(pathInfo));
  }
 
  private PomInfo examinePom(PathInfo pathInfo)  {
    Path path = pathInfo.getPath();
    try {
      return pomInspector.examine(path);
    } catch (PomInspectionException e) {
      throw new RuntimeException("Problem processing POM at "+path);
    }
  }
  private Metadata constructGroupMetadata(PomInfo pomInfo) {
    
    if(!pomInfo.isPlugin()) {
      return null;
    }
    Plugin plugin = new Plugin();
    plugin.setArtifactId(pomInfo.getArtifactId());
    plugin.setName(pomInfo.getName());
    plugin.setPrefix(pomInfo.getGoalPrefix());
    Metadata m = new Metadata();
    m.setModelVersion("1.1.0");
    m.setGroupId(pomInfo.getGroupId());
    MetadataUtils.plugins(m).add(plugin);
    return m;
  }
  private Metadata constructArtifactMetadata(PathInfo pathInfo) {
    Metadata m = new Metadata();
    m.setModelVersion("1.1.0");
    m.setGroupId(pathInfo.getGroupId());
    m.setArtifactId(pathInfo.getArtifactId());
    MetadataUtils.versions(m).add(pathInfo.getVersion());
    m.getVersioning().setLatest(pathInfo.getVersion());
    if(!pathInfo.getVersion().endsWith("-SNAPSHOT")) {
      m.getVersioning().setRelease(pathInfo.getVersion());
    }
   
    return m;
  }
  private Map<Path, Metadata> merge(Map<Path, Metadata> a, Map<Path, Metadata> b) {
    Map<Path, Metadata> base = a.size() > b.size() ? a : b;
    Map<Path, Metadata> other = a.size() > b.size() ? b : a;
    other.forEach((p, m) -> base.merge(p, m, mergeStrategy::merge));
    return base;
  }
  
  
  
}
