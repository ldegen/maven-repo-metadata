package de.ldegen.maven.repo.metadata;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public abstract class Builder<T, B extends Builder<T,?>> {


//unfortunately java is stupid, so we have to invent different names
  // for apply and bind.
  public B map(UnaryOperator<T> f) {
    return flatmap((v)->unit(f.apply(v)));
  }
  
  // unfortunately objects are stupid.
  public B modify(Consumer<T> c) {
    return flatmap((v)->{
      c.accept(v);
      return unit(v);
    });
  }
  
  public B flatmap(Function<T, B> f) {
    T copy = copy(get());
    return f.apply(copy);
  }
  protected abstract T get();

  protected abstract B unit(T v);

  protected abstract T copy(T orig);
}
