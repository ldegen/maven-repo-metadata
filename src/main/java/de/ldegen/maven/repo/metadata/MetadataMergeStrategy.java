package de.ldegen.maven.repo.metadata;

public interface MetadataMergeStrategy {

  Metadata merge(Metadata a, Metadata b);

}