package de.ldegen.maven.repo.metadata;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.function.UnaryOperator;

import de.ldegen.maven.repo.metadata.PomInfo.Packaging;

import org.apache.maven.model.Build;
import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.codehaus.plexus.util.xml.Xpp3Dom;

/**
 * this is intended for unit testing.
 * it does not cover the whole maven model, just the parts that
 * we need for our unit tests.
 * @author lukas
 *
 */
public class ModelBuilder extends Builder<ModelBuilderData, ModelBuilder> {

  public static class PluginBuilder extends Builder<Plugin, PluginBuilder> {

    final Plugin plugin;

    public PluginBuilder(Plugin v) {
      plugin = v;
    }

    @Override
    protected Plugin get() {
      return plugin;
    }

    @Override
    protected PluginBuilder unit(Plugin v) {
      return new PluginBuilder(v);
    }

    @Override
    protected Plugin copy(Plugin orig) {
      return plugin.clone();
    }

    public static PluginBuilder of(String groupId, String artifactId, String version) {
      Plugin p = new Plugin();
      p.setGroupId(groupId);
      p.setArtifactId(artifactId);
      p.setVersion(version);
      return new PluginBuilder(p);
    }

    public PluginBuilder config(String key, String value) {
      return modify(cp -> {
        if (cp.getConfiguration() == null) {
          cp.setConfiguration(new Xpp3Dom("configuration"));
        }
        Xpp3Dom cfg = (Xpp3Dom) cp.getConfiguration();
        Xpp3Dom prop = new Xpp3Dom(key);
        prop.setValue(value);
        cfg.addChild(prop);
      });
    }

  }

  final ModelBuilderData data;

  private ModelBuilder(ModelBuilderData model) {
    this.data = model;
  }

  public static ModelBuilder of(String groupId, String artifactId, String version) {
    Model m = new Model();
    m.setModelVersion("4.0.0");
    m.setArtifactId(artifactId);
    m.setGroupId(groupId);
    m.setVersion(version);
    ModelBuilderData data = new ModelBuilderData(m);
    if(version.endsWith("-SNAPSHOT")) {
      data.timestamp=MetadataUtils.TIMESTAMP_FOMATTER.format(LocalDateTime.now());
      data.buildNumber=42;
    }
    return new ModelBuilder(data);
  }

  public ModelBuilder packaging(Packaging p) {
    return modify(cp -> cp.model.setPackaging(p.toString()));
  }

  public ModelBuilder property(String key, String value) {
    return modify(cp -> cp.model.getProperties().setProperty(key, value));
  }

  public ModelBuilder plugin(String artifactId, UnaryOperator<PluginBuilder> op) {
    return plugin("org.apache.maven.plugins", artifactId, null, op);
  }

  public ModelBuilder plugin(String artifactId, String version, UnaryOperator<PluginBuilder> op) {
    return plugin("org.apache.maven.plugins", artifactId, version, op);
  }

  public ModelBuilder plugin(String groupId, String artifactId, String version, UnaryOperator<PluginBuilder> op) {
    PluginBuilder pb = PluginBuilder.of(groupId, artifactId, version);
    return modify(cp -> {
      if (cp.model.getBuild() == null) {
        cp.model.setBuild(new Build());
      }
      cp.model.getBuild().getPlugins().add(op.apply(pb).get());
    });
  }
  public ModelBuilder parent(String artifactId) {
    return parent(null,artifactId,null);
  }
  public ModelBuilder parent(String groupId, String artifactId, String version) {
    return modify(cp -> {
      Parent parent = new Parent();
      parent.setArtifactId(artifactId);
      parent.setGroupId(groupId==null ? cp.model.getGroupId() : groupId);
      parent.setVersion(version==null ? cp.model.getVersion() : version);
      cp.model.setParent(parent);
      if (cp.model.getGroupId() != null && cp.model.getGroupId().equals(parent.getGroupId())) {
        cp.model.setGroupId(null);
      }
      if (cp.model.getVersion() != null && cp.model.getVersion().equals(parent.getVersion())) {
        cp.model.setVersion(null);
      }
    });
  }
  
  public ModelBuilder timestamp(String ts) {
    return modify(cp->cp.timestamp=ts);
  }
  public ModelBuilder buildNumber(Integer bn) {
    return modify(cp->cp.buildNumber=bn);
  }
  
  @Override
  protected ModelBuilderData get() {
    return data;
  }

  @Override
  protected ModelBuilder unit(ModelBuilderData v) {
    return new ModelBuilder(v);
  }

  @Override
  protected ModelBuilderData copy(ModelBuilderData orig) {
    return new ModelBuilderData(orig);
  }

  public String buildString() throws IOException {
    StringWriter writer = new StringWriter();
    new MavenXpp3Writer().write(writer, data.model);
    return writer.toString();
  }
  
  public byte[] buildByteArray() throws IOException {
    ByteArrayOutputStream writer = new ByteArrayOutputStream();
    new MavenXpp3Writer().write(writer, data.model);
    return writer.toByteArray();
  }

  public ModelBuilder name(String string) {
    return modify(cp -> cp.model.setName(string));
  }

  public Model buildModel() {
    return data.model;
  }
  public PathInfo buildPathInfo() {
    
    return PathInfo.of(data.getGroupId(), data.model.getArtifactId(), data.getVersion(), data.timestamp, data.buildNumber).withExtension("pom");
  }
  public Path buildPath() {
    return buildPathInfo().getPath();
  }

  
 
}
class ModelBuilderData {

  public Model model;
  public String timestamp;
  public Integer buildNumber;

  public ModelBuilderData(ModelBuilderData orig) {
    this.model=orig.model.clone();
    this.timestamp=orig.timestamp; //immutable
    this.buildNumber=orig.buildNumber; //immutable
  }
  public ModelBuilderData(Model model) {
    this.model=model;
  }
  public String getVersion() {
    if(model.getVersion()!=null) {
      return model.getVersion();
    }
    if(model.getParent()!=null) {
      return model.getParent().getVersion();
    }
    return null;
  }
  public String getGroupId() {
    if(model.getGroupId()!=null) {
      return model.getGroupId();
    }
    if(model.getParent()!=null) {
      return model.getParent().getGroupId();
    }
    return null;
  }
}
