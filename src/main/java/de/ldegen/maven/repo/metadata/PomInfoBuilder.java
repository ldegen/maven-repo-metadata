package de.ldegen.maven.repo.metadata;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import de.ldegen.maven.repo.metadata.PomInfo.Packaging;

/**
 * this is intended for creation of test data for unit testing.
 * 
 * Note that this does not cover the whole thing, but only what we need in our
 * tests.
 * 
 * @author lukas
 *
 */
public class PomInfoBuilder extends Builder<PomInfoBuilderData, PomInfoBuilder> {

  public static PomInfoBuilder of(String groupId, String artifactId, String version) {
    PomInfo info = PomInfo.of(groupId, artifactId, version);

    PomInfoBuilderData data = new PomInfoBuilderData(info);
    if (version.endsWith("-SNAPSHOT")) {
      data.timestamp = MetadataUtils.TIMESTAMP_FOMATTER.format(LocalDateTime.now());
      data.buildNumber = 42;
    }
    return new PomInfoBuilder(data);
  }

  private final PomInfoBuilderData data;

  private PomInfoBuilder(PomInfoBuilderData data) {
    this.data = data;
  }

  @Override
  protected PomInfoBuilderData get() {
    return data;
  }

  @Override
  protected PomInfoBuilder unit(PomInfoBuilderData v) {
    return new PomInfoBuilder(v);
  }

  @Override
  protected PomInfoBuilderData copy(PomInfoBuilderData orig) {
    return new PomInfoBuilderData(orig);
  }

  public PomInfoBuilder packaging(Packaging packaging) {
    return modify(cp -> {
      cp.info = cp.info.withPackaging(packaging);
    });
  }

  public PomInfoBuilder name(String name) {
    return modify(cp -> {
      cp.info = cp.info.withName(name);
    });
  }

  public PomInfo buildPomInfo() {
    return data.info;
  }

  public Path buildPath() {
    return buildPathInfo().getPath();
  }

  private PathInfo buildPathInfo() {
    return PathInfo
        .of(data.info.getGroupId(), data.info.getArtifactId(), data.info.getVersion(), data.timestamp, data.buildNumber)
        .withExtension("pom");
  }
}

class PomInfoBuilderData {

  public PomInfo info;
  public String timestamp;
  public Integer buildNumber;

  public PomInfoBuilderData(PomInfoBuilderData orig) {
    this.info = orig.info; // immutable
    this.timestamp = orig.timestamp; // immutable
    this.buildNumber = orig.buildNumber; // immutable
  }

  public PomInfoBuilderData(PomInfo info) {
    this.info = info;
  }
}
