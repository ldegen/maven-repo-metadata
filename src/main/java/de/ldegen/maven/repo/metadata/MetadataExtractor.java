package de.ldegen.maven.repo.metadata;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.maven.model.Build;
import org.apache.maven.model.Model;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.building.DefaultModelBuilder;
import org.apache.maven.model.building.DefaultModelBuilderFactory;
import org.apache.maven.model.building.DefaultModelBuildingRequest;
import org.apache.maven.model.building.ModelBuildingException;
import org.apache.maven.model.building.ModelBuildingResult;
import org.apache.maven.model.building.ModelSource;
import org.apache.maven.model.building.StringModelSource;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.PlexusContainerException;
import org.codehaus.plexus.component.configurator.expression.ExpressionEvaluationException;
import org.codehaus.plexus.util.xml.Xpp3Dom;

public class MetadataExtractor implements PomInspector {

  private final class TrivialModelSource implements ModelSource {
    private final InputStream content;
    private final String location;

    private TrivialModelSource(InputStream content, String location) {
      this.content = content;
      this.location = location;
    }

    @Override
    public InputStream getInputStream() throws IOException {
      return content;
    }

    @Override
    public String getLocation() {
      return location;
    }
  }

  
  private PathBasedModelResolver modelResolver;
  private PathBasedStorage storage;

  public MetadataExtractor(PathBasedStorage storage) throws PomInspectionException {
    this.storage = storage;
    this.modelResolver = new PathBasedModelResolver(storage);
    
  }

  
  @Override
  public PomInfo examine(Path path) throws PomInspectionException {
    return processModelSource(PathBasedModelSource.lookup(storage, path));
  }
  
  public PomInfo examine(InputStream content) throws PomInspectionException {
    return examine(content,"");
  }
  public PomInfo examine(String content) throws PomInspectionException {
    return examine(content,"");
  }
  
  public PomInfo examine(InputStream content, String location) throws PomInspectionException {
    return processModelSource(new TrivialModelSource(content, location));
  }
  
  public PomInfo examine(String content, String location) throws PomInspectionException {
    return processModelSource(new StringModelSource(content, location));
  }


  private PomInfo processModelSource(ModelSource source) throws PomInspectionException {
    DefaultModelBuildingRequest moduleRequest = new DefaultModelBuildingRequest();
    DefaultModelBuilder dmb = new DefaultModelBuilderFactory().newInstance();
    moduleRequest.setModelResolver(modelResolver);
    moduleRequest.setModelSource(source);

    ModelBuildingResult moduleResult;
    try {
      moduleResult = dmb.build(moduleRequest);
    } catch (ModelBuildingException e) {
      throw new PomInspectionException("failed to build model",e);
    }
    Model effectiveModel = moduleResult.getEffectiveModel();
    MavenProject prj = new MavenProject(effectiveModel);
    
    String prefix;
    try {
      prefix = explicitPrefixMapping(prj).orElse(implicitPrefixMapping(prj.getArtifactId()));
    } catch (ExpressionEvaluationException | PlexusContainerException e) {
      throw new PomInspectionException("failed to detect plugin prefix mapping", e);
    }

    return PomInfo.of(prj.getGroupId(), prj.getArtifactId(), prj.getVersion())
        .withName(prj.getName())
        .withPackaging(prj.getPackaging())
        .withGoalPrefix(prefix);
  }
  
  

  private String implicitPrefixMapping(String artifactId) {
    Optional<Matcher> matcher = Stream.of(
      Pattern.compile("(.+)-maven-plugin"),
      Pattern.compile("maven-(.+)-plugin")
    ).map(p->p.matcher(artifactId)).filter(m->m.matches()).findFirst();
    return matcher.map(m->m.group(1)).orElse(null);
  }

  private Optional<String> explicitPrefixMapping(MavenProject prj) throws ExpressionEvaluationException, PlexusContainerException {
    return Optional
        .ofNullable(prj.getBuild())
        .map(Build::getPlugins)
        .flatMap(plugins -> plugins.stream().filter(MetadataExtractor::isPluginPlugin).findFirst())
        .flatMap(plugin -> {
          Xpp3Dom configuration = (Xpp3Dom) plugin.getConfiguration();
          Xpp3Dom child = configuration.getChild("goalPrefix");
          return Optional.ofNullable(child).map(c->c.getValue());
        });
  }

  

  public static boolean isPluginPlugin(Plugin p) {
    return p.getGroupId().equals("org.apache.maven.plugins") && p.getArtifactId().equals("maven-plugin-plugin");
  }

}
