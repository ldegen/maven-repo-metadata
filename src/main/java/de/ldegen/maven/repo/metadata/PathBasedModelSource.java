package de.ldegen.maven.repo.metadata;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;

import org.apache.maven.model.building.ModelSource2;

class PathBasedModelSource implements ModelSource2{

  private final Path path;
  private final PathBasedStorage storage;

  
  public static PathBasedModelSource lookup(PathBasedStorage storage, Path path) {
    Path checkedPath = checkPath(storage,path);
    if(checkedPath==null) {
      return null;
    }
    return new PathBasedModelSource(path,storage);
  }
  private static Path checkPath(PathBasedStorage storage, Path path) {
    Path normalizedPath = storage.isFile(path) ? path : path.resolve("pom.xml");
    if(!storage.isFile(normalizedPath)) {
      return null;
    }
    return normalizedPath;
  }
  
  private PathBasedModelSource(Path path, PathBasedStorage storage) {
    this.storage = storage;
    this.path=path;
  }

  @Override
  public InputStream getInputStream() throws IOException {
    return storage.load(path);
  }

  @Override
  public String getLocation() {
    return path.toString();
  }

  @Override
  public ModelSource2 getRelatedSource(String relPath) {
    //FIXME: how is ModelSource.resolveModel allowed to throw an exception, while this
    //method is not?
    Path resolvedPath = path.resolve(relPath).normalize();
    return PathBasedModelSource.lookup(storage, resolvedPath);
  }

  @Override
  public URI getLocationURI() {
    return path.toUri();
  }
  
}