package de.ldegen.maven.repo.metadata;

import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.ldegen.maven.repo.metadata.Metadata.Plugins;
import de.ldegen.maven.repo.metadata.Versioning.SnapshotVersions;
import de.ldegen.maven.repo.metadata.Versioning.Versions;

public class MetadataUtils {
  public static final String TIMESTAMP_PATTERN = "yyyyMMdd.HHmmss"; 
  public static final DateTimeFormatter TIMESTAMP_FOMATTER 
                    = DateTimeFormatter.ofPattern(TIMESTAMP_PATTERN);
  public static List<Plugin> plugins(Metadata metadata) {
    if(metadata.getPlugins()==null) {
      metadata.setPlugins(new Plugins());
    }
    return metadata.getPlugins().getPlugins();
  }

  public static List<String> versions(Versioning versioning) {
    if(versioning.getVersions()==null) {
      versioning.setVersions(new Versions());
    }
    return versioning.getVersions().getVersions();
  }
  public static List<String> versions(Metadata metadata) {
    return versions(versioning(metadata));
  }

  /**
   * Merge metadata.
   * The code is taken from the Maven-Codebase, {@link org.apache.maven.artifact.repository.metadata.Metadata#merge}
   * I only applied trivial modifications to make it fit into this package. I also renamed some local variables for readability.
   * 
   * FIXME: Why did we not just use the Maven-Classes? They are virtually identical (generated from the same model after all).
   * But since we will probably use the Metadata-Entities in our public interface, I preferred recreating those
   * in our own code base to not force another dependencie on our clients. Not sure if this is a good idea.
   * Can be changed.
   * 
   * The original algorithm seems to work on the assumption that there are two metadata records to be merged:
   * <ul>
   * <li> the current state that needs to be updated: in the original method, this was the method reciever, i.e. "self" or "this".
   *      I introduced the argument <code>target</code> for this.
   * <li> the new information that needs to be merged into the current state. This is the second argument <code>source</code>
   * </ul>
   * If conflicting entries exist in both records, the information in <code>source</code> is assumed to be more up to date
   * and will replace the corresponding entries in <code>target</code>
   */
  public static boolean mergeSourceIntoTarget( Metadata target, Metadata source )
  {
      boolean changed = false;
      for ( Plugin plugin : plugins(source))
      {
          boolean found = false;
  
          for ( Plugin preExisting : plugins(target) )
          {
              if ( preExisting.getPrefix().equals( plugin.getPrefix() ) )
              {
                  found = true;
                  break;
              }
          }
  
          if ( !found )
          {
              Plugin mappedPlugin = new Plugin();
  
              mappedPlugin.setArtifactId( plugin.getArtifactId() );
  
              mappedPlugin.setPrefix( plugin.getPrefix() );
  
              mappedPlugin.setName( plugin.getName() );
  
              target.getPlugins().getPlugins().add( mappedPlugin );
  
              changed = true;
          }
      }
  
      Versioning sourceVersioning = source.getVersioning();
      if ( sourceVersioning != null )
      {
          Versioning targetVersioning = target.getVersioning();
          if ( targetVersioning == null )
          {
              targetVersioning = new Versioning();
              target.setVersioning( targetVersioning );
              changed = true;
          }
  
          for ( String sourceVersion : versions(sourceVersioning) )
          {
              if ( !versions(targetVersioning).contains( sourceVersion ) )
              {
                  changed = true;
                  versions(targetVersioning).add( sourceVersion );
              }
          }
  
          if ( "null".equals( sourceVersioning.getLastUpdated() ) )
          {
              sourceVersioning.setLastUpdated( null );
          }
  
          if ( "null".equals( targetVersioning.getLastUpdated() ) )
          {
              targetVersioning.setLastUpdated( null );
          }
  
          if ( sourceVersioning.getLastUpdated() == null || sourceVersioning.getLastUpdated().length() == 0 )
          {
              // this should only be for historical reasons - we assume local is newer
              sourceVersioning.setLastUpdated( targetVersioning.getLastUpdated() );
          }
  
          if ( targetVersioning.getLastUpdated() == null || targetVersioning.getLastUpdated().length() == 0
               || sourceVersioning.getLastUpdated().compareTo( targetVersioning.getLastUpdated() ) >= 0 )
          {
              changed = true;
              targetVersioning.setLastUpdated( sourceVersioning.getLastUpdated() );
  
              if ( sourceVersioning.getRelease() != null )
              {
                  changed = true;
                  targetVersioning.setRelease( sourceVersioning.getRelease() );
              }
              if ( sourceVersioning.getLatest() != null )
              {
                  changed = true;
                  targetVersioning.setLatest( sourceVersioning.getLatest() );
              }
  
              Snapshot targetSnapshot = targetVersioning.getSnapshot();
              Snapshot sourceSnapshot = sourceVersioning.getSnapshot();
              if ( sourceSnapshot != null )
              {
                  if ( targetSnapshot == null )
                  {
                      targetSnapshot = new Snapshot();
                      targetVersioning.setSnapshot( targetSnapshot );
                      changed = true;
                  }
  
                  // overwrite
                  if ( targetSnapshot.getTimestamp() == null ? sourceSnapshot.getTimestamp() != null
                      : !targetSnapshot.getTimestamp().equals( sourceSnapshot.getTimestamp() ) )
                  {
                      targetSnapshot.setTimestamp( sourceSnapshot.getTimestamp() );
                      changed = true;
                  }
                  if ( targetSnapshot.getBuildNumber() != sourceSnapshot.getBuildNumber() )
                  {
                      targetSnapshot.setBuildNumber( sourceSnapshot.getBuildNumber() );
                      changed = true;
                  }
                  if ( targetSnapshot.isLocalCopy() != sourceSnapshot.isLocalCopy() )
                  {
                      targetSnapshot.setLocalCopy( sourceSnapshot.isLocalCopy() );
                      changed = true;
                  }
              }
          }
      }
      return changed;
  }

  /**
   * basic sanity checks to see if the two metadata records actually describe the same thing.
   * @param a
   * @param b
   */
  public static void checkMergable(Metadata a, Metadata b) {
    // artifact id should either be set for both records, or it should be absent
    // from both.
    if (
      (a.getArtifactId() != null && b.getArtifactId() == null || a.getArtifactId() == null && b.getArtifactId() != null)
    ) {
      throw new IllegalArgumentException("artifactId must either be present in both records or in none");
    }
    // same for the version (used for snapshots only)
    if ((a.getVersion() != null && b.getVersion() == null || a.getVersion() == null && b.getVersion() != null)) {
      throw new IllegalArgumentException("version must either be present in both records or in none");
    }
  
    // groupId must always be present
    if (a.getGroupId() == null || b.getGroupId() == null) {
      throw new IllegalArgumentException("groupId must be present in both records");
    }
  
    // if two metadata files are given for the same (logical) path,
    // they should refer to the same group or artifact.
  
    if (!a.getGroupId().equals(b.getGroupId())) {
      throw new IllegalArgumentException("cannot merge metadata records for different groupIds");
    }
  
    // if present, the artifactIds must match as well.
    if (a.getArtifactId() != null && !a.getArtifactId().equals(b.getArtifactId())) {
      throw new IllegalArgumentException("cannot merge metadata records for different artifacts");
    }
    // if a version is present, so should be the artifactId
    if (a.getVersion() != null && a.getArtifactId() == null) {
      throw new IllegalArgumentException("records cannot have a version without an artifactId");
    }
    // if present, the versions must match as well.
    if (a.getVersion() != null && !a.getVersion().equals(b.getVersion())) {
      throw new IllegalArgumentException("cannot merge metadata records for different versions");
    }
  }

  public static Versions versions(List<String> versionNumbers) {
    Versions versions = new Versions();
    versions.getVersions().addAll(versionNumbers);
    return versions;
  }
  
  public static Metadata deepCopy(Metadata orig) {
    Metadata copy = new Metadata();
    copy.setArtifactId(orig.getArtifactId());
    copy.setGroupId(orig.getGroupId());
    copy.setModelVersion(orig.getModelVersion());
    copy.setVersion(orig.getVersion());
    copy.setPlugins(deepCopy(orig.getPlugins()));
    copy.setVersioning(deepCopy(orig.getVersioning()));
    return copy;
  }

  public static Versioning deepCopy(Versioning orig) {
    if(orig==null) {
      return null;
    }
    Versioning copy = new Versioning();
    copy.setLastUpdated(orig.getLastUpdated());
    copy.setLatest(orig.getLatest());
    copy.setRelease(orig.getRelease());
    copy.setSnapshot(deepCopy(orig.getSnapshot()));
    copy.setSnapshotVersions(deepCopy(orig.getSnapshotVersions()));
    copy.setVersions(deepCopy(orig.getVersions()));
    return copy;
  }

  public static Versions deepCopy(Versions orig) {
    if(orig==null) {
      return null;
    }
    
    Versions copy = new Versions();
    if(orig.versions!=null) {
      copy.getVersions().addAll(orig.getVersions());
    }
    return copy;
  }

  public static SnapshotVersions deepCopy(SnapshotVersions orig) {
    if(orig==null) {
      return null;
    }
    SnapshotVersions copy = new SnapshotVersions();
    if(orig.snapshotVersions!=null) {
      for(SnapshotVersion origSnapshotVersion : orig.getSnapshotVersions()) {
        copy.getSnapshotVersions().add(deepCopy(origSnapshotVersion));
      }
    }
    return copy;
  }

  public static SnapshotVersion deepCopy(SnapshotVersion orig) {
    if(orig==null) {
      return null;
    }
    SnapshotVersion copy = new SnapshotVersion();
    copy.setClassifier(orig.getClassifier());
    copy.setExtension(orig.getExtension());
    copy.setUpdated(orig.getUpdated());
    copy.setValue(orig.getValue());
    return copy;
  }

  public static Snapshot deepCopy(Snapshot orig) {
    if(orig==null) {
      return null;
    }
    Snapshot copy = new Snapshot();
    copy.setBuildNumber(orig.getBuildNumber());
    copy.setLocalCopy(orig.isLocalCopy());
    copy.setTimestamp(orig.getTimestamp());
    return copy;
    
  }

  public static Plugins deepCopy(Plugins orig) {
    if(orig==null) {
      return null;
    }
    
    Plugins copy = new Plugins();
    if(orig.plugins!=null) {
      for(Plugin origPlugin:orig.getPlugins()) {
        copy.getPlugins().add(deepCopy(origPlugin));
      }
    }
    return copy;
  }

  public static Plugin deepCopy(Plugin orig) {
    if (orig==null) {
      return null;
    }
    Plugin copy = new Plugin();
    copy.setArtifactId(orig.getArtifactId());
    copy.setName(orig.getName());
    copy.setPrefix(orig.getPrefix());
    return copy;
  }

  public static Snapshot snapshot(Metadata md) {
    Versioning versioning = versioning(md);
    if(versioning.getSnapshot()==null) {
      versioning.setSnapshot(new Snapshot());
    }
    return versioning.getSnapshot();
  }

  public static Versioning versioning(Metadata md) {
    if(md.getVersioning()==null) {
      md.setVersioning(new Versioning());
    }
    return md.getVersioning();
  }
  
  public static List<SnapshotVersion> snapshotVersions(Versioning versioning){
    if(versioning.getSnapshotVersions()==null) {
      versioning.setSnapshotVersions(new SnapshotVersions());
    }
    return versioning.getSnapshotVersions().getSnapshotVersions();
  }
  public static List<SnapshotVersion> snapshotVersions(Metadata md){
    return snapshotVersions(versioning(md));
  }

  public static Path metadataPath(Metadata metadata) {
    String groupId = metadata.getGroupId();
    String artifactId = metadata.getArtifactId();
    String version = metadata.getVersion();
    return metadataPath(groupId, artifactId, version);
  }

  public static Path metadataPath(String groupId, String artifactId, String version) {
    if(groupId==null) {
      throw new IllegalArgumentException("groupId must always be set");
    }
    Path path = Path.of("",groupId.split("\\."));
    if(artifactId!=null) {
      path = path.resolve(artifactId);
    }
    if(version!=null) {
      if(artifactId==null) {
        throw new IllegalArgumentException("if version is set, then artifactId must be set aswell");
      }
      path = path.resolve(version);
    }
    return path.resolve("maven-metadata.xml");
  }

  public static int compareSnapshotVersions(SnapshotVersion sva, SnapshotVersion svb) {
    return new ComparableVersion(sva.getValue()).compareTo(new ComparableVersion(svb.getValue()));
  }

  public static Path buildPath(
      String groupId, String artifactId, String version, String timestamp, Integer buildNumber, String classifier,
      String extension
  ) {
    String[] groupParts = groupId.split("\\.");
    Path groupPath = Path.of("", groupParts);
    Path versionPath = groupPath.resolve(Path.of(artifactId, version));
    String subversion = (version.endsWith("-SNAPSHOT")
        ? version.substring(0,version.length()-"-SNAPSHOT".length())+"-"+timestamp+"-"+buildNumber
        : version);
    String filename = artifactId + "-" + subversion +(classifier==null?"":"-"+classifier)+ "."+extension;
    return versionPath.resolve(filename);
  }
}
