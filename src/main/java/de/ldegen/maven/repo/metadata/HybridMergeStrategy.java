package de.ldegen.maven.repo.metadata;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.maven.artifact.versioning.ComparableVersion;

/**
 * this strategy uses
 * {@link MetadataUtils#mergeSourceIntoTarget(Metadata, Metadata)}, which in
 * turn copies the behaviour of
 * {@link org.apache.maven.artifact.repository.metadata.Metadata#merge}.
 * 
 * We deviate from the original behavior when it comes to determining ordering
 * of versions and determining the values for the <code>release</code> and
 * <code>latest</code> fields. Other than the original algorithm, we cannot
 * assume that one of the records is more "up to date" than the other. The main
 * reason for this is, that we want to use our implementation in situations like
 * incremental scanning of a repository for metadata, where things like
 * timestamps, even when available, do not make a terrible lot of sense.
 * 
 * @author lukas
 *
 */
public class HybridMergeStrategy implements MetadataMergeStrategy {
  @Override
  public Metadata merge(Metadata a, Metadata b) {
    if (a == null) {
      return b;
    }
    if (b == null) {
      return a;
    }

    MetadataUtils.checkMergable(a, b);

    Metadata c = new Metadata();
    c.setArtifactId(a.getArtifactId());
    c.setGroupId(a.getGroupId());
    c.setModelVersion("1.1.0"); // TODO: that's the only version I know of?
    c.setVersion(a.getVersion());

    MetadataUtils.mergeSourceIntoTarget(c, a);
    MetadataUtils.mergeSourceIntoTarget(c, b);

    if(a.getVersioning()==null && b.getVersioning()==null){
      return c;
    }
    if (c.getVersioning() == null) {
      c.setVersioning(new Versioning());
    }
    // we override the logic for determining the <latest> and <release> elements,
    Stream<String> avs = MetadataUtils.versions(a).stream();
    Stream<String> bvs = MetadataUtils.versions(b).stream();

    List<String> mergedVersions = Stream.concat(avs, bvs).map(ComparableVersion::new).sorted().distinct()
        .map(ComparableVersion::toString).collect(Collectors.toList());
    if (!mergedVersions.isEmpty()) {
      c.getVersioning().setVersions(MetadataUtils.versions(mergedVersions));
      c.getVersioning().setLatest(mergedVersions.get(mergedVersions.size() - 1));
      c.getVersioning().setRelease(
          mergedVersions.stream().filter(s -> !s.endsWith("-SNAPSHOT")).reduce((aa, bb) -> bb).orElse(null)
      );
    }

    return c;
  }
}
