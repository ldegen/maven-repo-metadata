package de.ldegen.maven.repo.metadata;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.HashMap;

/** mainly intended for unit tests */
public class InMemoryStorage implements PathBasedStorage{

  private static class Entry{
    public Entry(Path path) {
      this(path,null);
    }
    public Entry(Path path, byte[] content) {
      this.path = path;
      this.content = content;
    }
    final Path path;
    final byte[] content;
    
  }
  
  protected HashMap<Path, Entry> store= new HashMap<>();
  
  
  public void put(Path path, byte[] content) {
    mkdirDashP(path.getParent());
    store.put(path, new Entry(path,content));
  }
  public void put(MetadataBuilder mdb) {
    put(mdb.buildPath(),mdb.buildByteArray());
  }
  public void put(ModelBuilder mb) throws IOException {
    put(mb.buildPath(),mb.buildByteArray());
  }
  
  
  private void mkdirDashP(Path path) {
    if(path == null||store.containsKey(path)) {
      return;
    }
    mkdirDashP(path.getParent());
    store.put(path, new Entry(path));
  }

  @Override
  public boolean exists(Path path) {
    return store.containsKey(path);
  }

  @Override
  public boolean isFile(Path path) {
    Entry entry = store.get(path);
    return entry!=null && entry.content != null;
  }

  @Override
  public boolean isDirectory(Path path) {
    Entry entry = store.get(path);
    return entry!=null && entry.content == null;
  }

  @Override
  public InputStream load(Path path) {
    Entry entry = store.get(path);
    return entry==null?null:new ByteArrayInputStream(entry.content);
  }
  

}
