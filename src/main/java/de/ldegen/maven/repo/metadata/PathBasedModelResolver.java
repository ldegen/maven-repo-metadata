package de.ldegen.maven.repo.metadata;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

import javax.xml.bind.JAXB;

import org.apache.maven.model.building.ModelSource;
import org.apache.maven.model.resolution.UnresolvableModelException;

/**
 * A {@link PathBasedModelResolver} is a {@link StatelessModelResolver} that
 * works by constructing Path-Strings from artefact coordinates and looks those
 * paths up using a {@link PathBasedStorage}
 * 
 * @author lukas
 *
 */
public class PathBasedModelResolver extends StatelessModelResolver {

  private final PathBasedStorage storage;
  private final Path root;

  public PathBasedModelResolver(PathBasedStorage storage) {
    this(storage, null);
  }

  public PathBasedModelResolver(PathBasedStorage storage, Path root) {
    this.storage = storage;
    this.root = root;
  }

  @Override
  public ModelSource resolveModel(String groupId, String artifactId, String version) throws UnresolvableModelException {
    final Path path = constructPath(groupId, artifactId, version);
    PathBasedModelSource source = PathBasedModelSource.lookup(storage, path);
    if (source == null) {
      throw new UnresolvableModelException("cannot load "+path+" from storage", groupId, artifactId, version);
    }
    return source;
  }

  protected Path constructPath(String groupId, String artifactId, String version) throws UnresolvableModelException {
    String[] groupParts = groupId.split("\\.");
    Path groupPath = Path.of("", groupParts);
    Path versionPath = groupPath.resolve(Path.of(artifactId, version));
    String subversion = (version.endsWith("-SNAPSHOT")
        ? resolveSnapshotVersion(groupId, artifactId, version)
        : version);

    Path rel = versionPath.resolve(artifactId + "-" + subversion + ".pom");
    return root == null ? rel : root.resolve(rel);
  }

  protected String resolveSnapshotVersion(String groupId, String artifactId, String version)
      throws UnresolvableModelException {
    Path metadataPath = MetadataUtils.metadataPath(groupId, artifactId, version);
    InputStream load = storage.load(metadataPath);
    if (load == null) {
      throw new UnresolvableModelException("no metadata found for snapshot version", groupId, artifactId, version);
    }
    Metadata md = JAXB.unmarshal(load, Metadata.class);

    Optional<String> maxValue = MetadataUtils.snapshotVersions(md).stream()
        .filter(sv -> "pom".equals(sv.getExtension())).max(MetadataUtils::compareSnapshotVersions).map(SnapshotVersion::getValue);

    String targetVersion = version.substring(0, version.length() - "-SNAPSHOT".length());
    return maxValue
        .or(
            () -> Optional.ofNullable(md.getVersioning()).map(Versioning::getSnapshot)
                .map(s -> targetVersion + "-" + s.getTimestamp() + "-" + s.getBuildNumber())
        )
        .orElseThrow(
            () -> new UnresolvableModelException(
                "no version for " + version + " found in snapshot metadata", groupId, artifactId, version
            )
        );
  }

}
