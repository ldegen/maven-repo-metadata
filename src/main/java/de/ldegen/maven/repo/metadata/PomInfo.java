package de.ldegen.maven.repo.metadata;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * information obtained by looking at the contents of an effective POM.
 * <br>
 * Note: most of this information can also be obtained by looking at the path of an artifact, which is typically
 * much simpler. Have a look at {@link PathInfo}.
 * @author lukas
 *
 */
public class PomInfo {

  private final String version;
  private final String groupId;
  private final String artifactId;
  private final String name;
  private final Packaging packaging;
  private final String goalPrefix;

  public static enum Packaging {
    pom, jar, plugin("maven-plugin"), ejb, war, ear, rar;

    final private String value;

    Packaging() {
      this.value = this.name();
    }

    Packaging(String value) {
      this.value = value;
    }

    public static Packaging of(String s) {
      if (s.equals("maven-plugin")) {
        return plugin;
      } else {
        Packaging p = Enum.valueOf(Packaging.class,s);
        return p;
      }
    }

    @Override
    public String toString() {
      return value;
    }
  }

  public PomInfo withName(String name) {
    return new PomInfo(groupId, artifactId, version, name, packaging, goalPrefix);
  }

  public PomInfo withPackaging(String packaging) {
    return withPackaging(Packaging.of(packaging));
  }

  public PomInfo withPackaging(Packaging packaging) {
    if(packaging!=Packaging.plugin && goalPrefix!=null) {
      throw new IllegalStateException("you specified a goalPrefix '"+goalPrefix+"', so your packaging MUST be 'maven-plugin' and not '"+packaging+"'");
    }
    return new PomInfo(groupId, artifactId, version, name, packaging, null);
  }

  public PomInfo withGoalPrefix(String goalPrefix) {
    if( packaging != Packaging.plugin && goalPrefix != null) {
      throw new IllegalStateException("you specified a goalPrefix of '"+goalPrefix+"' for an artifact with packaging '"+packaging+"', but a goal prefix is only allowed for packaging 'maven-plugin'");
    }
    return new PomInfo(groupId, artifactId, version, name, packaging, goalPrefix);
  }

  public static PomInfo of(String groupId, String artifactId, String version) {
    return new PomInfo(groupId, artifactId, version, null, null, null);
  }

  private PomInfo(
      String groupId, String artifactId, String version, String name, Packaging packaging, String goalPrefix
  ) {
    this.groupId = groupId;
    this.artifactId = artifactId;
    this.version = version;
    this.name = name;
    this.packaging = packaging;
    this.goalPrefix = goalPrefix;
  }

  public static String implicitPrefix(String artifactId) {
    Optional<Matcher> matcher = Stream.of(Pattern.compile("(.+)-maven-plugin"), Pattern.compile("maven-(.+)-plugin"))
        .map(p -> p.matcher(artifactId)).filter(m -> m.matches()).findFirst();
    return matcher.map(m -> m.group(1)).orElse(null);
  }

  public String getVersion() {
    return version;
  }

  public String getGroupId() {
    return groupId;
  }

  public String getArtifactId() {
    return artifactId;
  }

  public String getName() {
    return name;
  }

  public Packaging getPackaging() {
    return (packaging == null ? Packaging.jar : packaging);
    
  }

  public String getGoalPrefix() {
    return (goalPrefix == null && packaging==Packaging.plugin ? implicitPrefix(artifactId) : goalPrefix);
  }

  public boolean isSnapshot() {
    return version.toUpperCase().endsWith("SNAPSHOT");
  }

  public boolean isPlugin() {
    return getPackaging()==Packaging.plugin;
  }

}
