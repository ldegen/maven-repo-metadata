package de.ldegen.maven.repo.metadata;

import java.io.InputStream;
import java.nio.file.Path;

/**
 * This interface defines the metadata-extractor's current assumptions
 * on how the underlying storage system works.
 * 
 * I had a look at https://github.com/Haven-King/reposilite/blob/nio/reposilite-backend/src/main/java/org/panda_lang/reposilite/storage/StorageProvider.java
 * which I was told may be the the abstraction used in Reposilite in the not-too-far future.
 * I am trying to view my own interface as a subset, so it will be easy to write an adapter later.
 * 
 * As the name suggests, we expect the storage to be organized around the concept of
 * Paths (like file system paths, or URIs, or something like that).
 * At the moment we do not require write access, and we do not require directory scanning.
 * 
 * Write access may be a later option to actually store maven-metadata.xml files, though
 * I have not yet decided on where this should happen.
 * Directory scanning may also become interesting at some point. But as with write access, I am not
 * sure yet, where to put this in the general scheme of things.
 * 
 * 
 * @author lukas
 *
 */
public interface PathBasedStorage {
  public boolean exists(Path path);
  public boolean isFile(Path path);
  public boolean isDirectory(Path path);
  public InputStream load(Path path);
}
